@echo off
echo ==== INICJUJE output.csv
copy naglowek.txt output.csv

for /f %%t in (lista.txt) do (
  echo ======== SEGMENTUJE %%t
  segment.exe -separate -input %%t

  for %%v in (%%t????.bmp) do (
    echo ================ LICZY CECHY %%v
    mazdacl -Pa %%v Roi_%%v featureset.txt output.csv %%t
  )
  
  del %%t????.bmp
  del Roi_%%t????.bmp
)


