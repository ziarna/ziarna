#ifndef _GABORFEATURES_H
#define _GABORFEATURES_H

#include <stdint.h>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc_c.h"

extern int mzCountOfGaborFeatures(void);
extern unsigned int mzNamesOfGaborFeatures(unsigned int index, char* buffer, unsigned int buffersize);
//extern void mzComputeGaborFeatures(double* features, int area, IplImage* src, int referx, int refery);
double mzComputeGaborFeature(unsigned int feature, unsigned int width, unsigned int height, const uint8_t* input, unsigned int linelength, int posx, int posy);
double mzComputeFFTGaborFeature(int feature, IplImage * img, int posx, int posy);

//extern void mzComputeGaborFeatures(double* features, unsigned int width, unsigned int height, const uint8_t* input, unsigned int linelength, int posx, int posy);
//extern void mzComputeFastGaborFeatures(double* features, unsigned int width, unsigned int height, const uint8_t* input, unsigned int linelength, int posx, int posy);

#endif //_GABORFEATURES_H