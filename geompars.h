
#ifndef _GEOMPARS_H
#define _GEOMPARS_H

void GeomFeaturesInvalidate(void);
double ComputeGeomFeature(const unsigned int x, const unsigned int y, char* name, IplImage* roi);

#endif //_GEOMPARS_H