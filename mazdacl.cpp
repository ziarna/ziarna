/*
* MaZdaCl - computes image texture, shape and color descriptors
*
* Copyright 1998-2013 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/


//#include <windows.h>
//#include <stdint.h>

#include <string.h>
#include <stdio.h>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#define _USE_MATH_DEFINES
#include <math.h>

#include "featuresmz.h"
#include "gabor.h"
#include "geompars.h"

struct MZ_BITMAPFILEHEADER {
        u_int16_t   bfType;
        u_int32_t   bfSize;
        u_int16_t   bfReserved1;
        u_int16_t   bfReserved2;
        u_int32_t   bfOffBits;
};// BITMAPFILEHEADER;

struct MZ_BITMAPINFOHEADER{
        u_int32_t      biSize;
        int32_t       biWidth;
        int32_t       biHeight;
        u_int16_t      biPlanes;
        u_int16_t      biBitCount;
        u_int32_t      biCompression;
        u_int32_t      biSizeImage;
        int32_t       biXPelsPerMeter;
        int32_t       biYPelsPerMeter;
        u_int32_t      biClrUsed;
        u_int32_t      biClrImportant;
};// BITMAPINFOHEADER;



#define MAX_ROIS 16
#define MAX_LINE 0x100

const double mznan = HUGE_VAL;
bool validRoiChannelMethod = false;
double features[32];


const unsigned int RoiColors[MAX_ROIS]=
	{
		0x000000FF, 0x0000FF00, 0x00FF0000,
		0x00FFFF00, 0x00FF00FF, 0x0000FFFF,
		0x000080FF, 0x008000FF, 0x0000FF80,
		0x0080FF00, 0x00FF0080, 0x00FF8000,
		0x0000C4FF, 0x0000FFC4, 0x00FF00C4,
		0x00C4FF00
//		,0x00F0F0F0
	};
const char separator[] = " ,;-\t\r\n/";

/////////////////////////////////////////////////////////////////////
//                       Specified
/////////////////////////////////////////////////////////////////////

double ComputeFeatureRoiMethodHistogram(IplImage* src, const unsigned int x, const unsigned int y, char* name, IplImage* roi, int area)
{
	char buf[32];
	int index = -1;
	int count = mzCountOfHistogramFeatures();
	for(int i = 0; i < count; i++)
	{
		mzNamesOfHistogramFeatures(i, buf, sizeof(buf));
		if(strcmp(buf, name) == 0) 
		{
			index = i;	
			break;
		}
	}
	if(index < 0) return mznan;
	if(!validRoiChannelMethod)
	{
		for(int i = 0; i < count; i++) features[i] = mznan;
		validRoiChannelMethod = true;
		mzComputeHistogramFeatures(features, area, src->width, src->height, (uint8_t *)src->imageData, src->widthStep, 
										(uint8_t *)roi->imageData, roi->width, roi->height, x, y, roi->widthStep);
	}
	return features[index];			
}
double ComputeFeatureRoiMethodGradient(IplImage* dst, const unsigned int x, const unsigned int y, char* name, IplImage* roi, int area, int min, int max, int bpp)
{
	char buf[32];
	int index = -1;
	int count = mzCountOfGradientFeatures();
	for(int i = 0; i < count; i++)
	{
		mzNamesOfGradientFeatures(i, buf, sizeof(buf));
		if(strcmp(buf, name) == 0) 
		{
			index = i;	
			break;
		}
	}
	if(index < 0) return mznan;
	if(!validRoiChannelMethod)
	{
		for(int i = 0; i < count; i++) features[i] = mznan;
		validRoiChannelMethod = true;
		mzComputeGradientFeatures(features, area, 
										bpp, min, max, 
										dst->width, dst->height, (uint8_t *)dst->imageData, dst->widthStep, 
										(uint8_t *)roi->imageData, roi->width, roi->height, x, y, roi->widthStep);
	}
	return features[index];			
}
double ComputeFeatureRoiMethodCom(IplImage* dst, const unsigned int x, const unsigned int y, char* name, IplImage* roi, int area, int min, int max, int bpp, bool optComSym)
{
	static int lastdire = -1;
	static int lastdist = -1;
	int dire;
	for(dire = 7; dire >= 0; dire--)
	{
		if(name[0] == mzDirections[dire]) break;
	}
	int dist = 1;
	switch(name[1])
	{
		case '1': dist = 1;  break;
		case '2': dist = 2;  break;
		case '3': dist = 3;  break;
		case '4': dist = 4;  break;
		case '5': dist = 5;  break;
		case '6': dist = 6;  break;
		case '7': dist = 7;  break;
		case '8': dist = 8;  break;
		case '9': dist = 9;  break;
	}

	if(lastdire != dire || lastdist != dist)
	{
		lastdire = dire;
		lastdist = dist;
		validRoiChannelMethod = false;
	}

	char buf[32];
	int index = -1;
	int count = mzCountOfComFeatures();
	for(int i = 0; i < count; i++)
	{
		mzNamesOfComFeatures(i, buf, sizeof(buf));
		if(strcmp(buf, name+2) == 0) 
		{
			index = i;	
			break;
		}
	}
	if(index < 0) return mznan;
	if(!validRoiChannelMethod)
	{
		for(int i = 0; i < count; i++) features[i] = mznan;
		validRoiChannelMethod = true;

		mzComputeComFeatures(features, bpp, min, max, optComSym, dist*mzVectors[dire][0], dist*mzVectors[dire][1],
						  dst->width, dst->height, (uint8_t *)dst->imageData, dst->widthStep,  
						  (uint8_t *)roi->imageData, roi->width, roi->height, x, y, roi->widthStep);
	}
	return features[index];			
}


double ComputeFeatureRoiMethodRlm(IplImage* dst, const unsigned int x, const unsigned int y, char* name, IplImage* roi, int area, int min, int max, int bpp)
{
	static int lastdire = -1;
	int dire;
	for(dire = 3; dire >= 0; dire--)
	{
		if(name[0] == mzDirections[dire]) break;
	}
	
	if(lastdire != dire)
	{
		lastdire = dire;
		validRoiChannelMethod = false;
	} 
	char buf[32];
	int index = -1;
	int count = mzCountOfRlmFeatures();
	for(int i = 0; i < count; i++)
	{
		mzNamesOfRlmFeatures(i, buf, sizeof(buf));
		if(strcmp(buf, name+1) == 0) 
		{
			index = i;	
			break;
		}
	}
	if(index < 0) return mznan;
	if(!validRoiChannelMethod)
	{
		for(int i = 0; i < count; i++) features[i] = mznan;
		validRoiChannelMethod = true;
		mzComputeRlmFeatures(features, area,
					  bpp, min, max, mzDirections[dire],
					  dst->width, dst->height, (uint8_t *)dst->imageData, dst->widthStep,  
					  (uint8_t *)roi->imageData, roi->width, roi->height, x, y, roi->widthStep);
	}
	return features[index];			
}

double ComputeFeatureRoiMethodArm(IplImage* dst, const unsigned int x, const unsigned int y, char* name, IplImage* roi)
{
	char buf[32];
	int index = -1;
	int count = mzCountOfArmFeatures();
	for(int i = 0; i < count; i++)
	{
		mzNamesOfArmFeatures(i, buf, sizeof(buf));
		if(strcmp(buf, name) == 0) 
		{
			index = i;	
			break;
		}
	}
	if(index < 0) return mznan;
	
	if(!validRoiChannelMethod)
	{
		for(int i = 0; i < count; i++) features[i] = mznan;
		validRoiChannelMethod = true;
		mzComputeArmFeatures(features, 
						dst->width, dst->height, (uint8_t *)dst->imageData, dst->widthStep, 
						(uint8_t *)roi->imageData, roi->width, roi->height, x, y, roi->widthStep);
	}
	return features[index];			
}
/*
double ComputeFeatureRoiMethodGabor(IplImage* dst, const unsigned int x, const unsigned int y, char* name, IplImage* roi)
{
	char buf[32];
	int index = -1;
	int count = mzCountOfGaborFeatures();
	for(int i = 0; i < count; i++)
	{
		mzNamesOfGaborFeatures(i, buf, sizeof(buf));
		if(strcmp(buf, name) == 0) 
		{
			index = i;	
			break;
		}
	}
	if(index < 0) return mznan;

	if(!validRoiChannelMethod)
	{
		for(int i = 0; i < count; i++) features[i] = mznan;
		validRoiChannelMethod = true;
		mzComputeGaborFeatures(features, dst->width, dst->height, (uint8_t *)dst->imageData, dst->widthStep,  x, y);
	}
	return features[index];			
}
*/
double ComputeFeatureRoiMethodFFTGabor(IplImage* dst, const unsigned int x, const unsigned int y, char* name, IplImage* roi)
{
	char buf[32];
	int index = -1;
	static int lastindex = -2;
	int count = mzCountOfGaborFeatures();
	for(int i = 0; i < count; i++)
	{
		mzNamesOfGaborFeatures(i, buf, sizeof(buf));
		if(strcmp(buf, name) == 0) 
		{
			index = i;	
			break;
		}
	}
	if(index < 0) return mznan;
	if(validRoiChannelMethod && lastindex == index)
	{
		validRoiChannelMethod = true;
		lastindex = index;
		return mzComputeFFTGaborFeature(-1, dst,  x, y);
	}
	else
	{
		validRoiChannelMethod = true;
		lastindex = index;
		return mzComputeFFTGaborFeature(index, dst,  x, y);
	}
}
double ComputeFeatureRoiMethodGabor(IplImage* dst, const unsigned int x, const unsigned int y, char* name, IplImage* roi)
{
	char buf[32];
	int index = -1;
	int count = mzCountOfGaborFeatures();
	for(int i = 0; i < count; i++)
	{
		mzNamesOfGaborFeatures(i, buf, sizeof(buf));
		if(strcmp(buf, name) == 0) 
		{
			index = i;	
			break;
		}
	}
	if(index < 0) return mznan;
	validRoiChannelMethod = true;
	return mzComputeGaborFeature(index, dst->width, dst->height, (uint8_t *)dst->imageData, dst->widthStep,  x, y);
}

double ComputeFeatureRoiMethod(IplImage* src, const unsigned int x, const unsigned int y, char* name, IplImage* roi, int area, int min, int max, int bpp)
{
	static char lastm = 0;
	if(lastm != name[0])
	{
		lastm = name[0];
		validRoiChannelMethod = false;
	}
	switch(name[0])
	{
	case 'T':
		return ComputeFeatureRoiMethodGabor(src, x, y, name+1, roi);
	case 'F':
		return ComputeFeatureRoiMethodFFTGabor(src, x, y, name+1, roi);
	case 'A':
		return ComputeFeatureRoiMethodArm(src, x, y, name+1, roi);
	case 'R':
		return ComputeFeatureRoiMethodRlm(src, x, y, name+1, roi, area, min, max, bpp);
	case 'S':
		return ComputeFeatureRoiMethodCom(src, x, y, name+1, roi, area, min, max, bpp, true);
	case 'C':
		return ComputeFeatureRoiMethodCom(src, x, y, name+1, roi, area, min, max, bpp, false);
	case 'G':
		return ComputeFeatureRoiMethodGradient(src, x, y, name+1, roi, area, min, max, bpp);
	case 'H':
		return ComputeFeatureRoiMethodHistogram(src, x, y, name+1, roi, area);
	default: return mznan;
	}
}
double ComputeFeatureRoiSpan(IplImage* dst, const unsigned int x, const unsigned int y, char* name, IplImage* roi)
{
	static int areal = 1;
	static int min;
	static int max;
	static char lastoptSpan = '+';
	static int lastbpp = -1;
	static unsigned int lastx = (unsigned int)(-1);
	static unsigned int lasty = (unsigned int)(-1);

	char* forward = name;
	char optSpan = ' ';
	int bpp = 8;
	
	switch(name[0])
	{
	case 'D': 
		optSpan = 'D';
		forward++;
		break;
	case 'S':
		optSpan = 'S';
		forward++;
		break;
	case 'N':
		optSpan = 'N';
		forward++;
		break;
	}
	if(optSpan != ' ')
	{
		switch(name[1])
		{
			case '1': bpp = 1; forward++; break;
			case '2': bpp = 2; forward++; break;
			case '3': bpp = 3; forward++; break;
			case '4': bpp = 4; forward++; break;
			case '5': bpp = 5; forward++; break;
			case '6': bpp = 6; forward++; break;
			case '7': bpp = 7; forward++; break;
			case '8': bpp = 8; forward++; break;
		}
	}
	else
	{
		optSpan = 'D';
		bpp = 8;
	}
	if((lastoptSpan != optSpan) || (lastbpp != bpp) || (!validRoiChannelMethod) || (lastx != x) || (lasty != y))
	{
		lastoptSpan = optSpan;
		lastbpp = bpp;
		lastx = x; 
		lasty = y;	
		validRoiChannelMethod = false;
		areal = mzComputeSpan(&min, &max, optSpan, dst->width, dst->height, (uint8_t *)dst->imageData, dst->widthStep, 
				(uint8_t *)roi->imageData, roi->width, roi->height, x, y, roi->widthStep);
	}
//	validRoiChannelMethod = false;
	
	if(areal <= 0 || min >= max) return mznan; 
	return ComputeFeatureRoiMethod(dst, x, y, forward, roi, areal, min, max, bpp);
}

double ComputeFeatureRoi(IplImage* src, const unsigned int x, const unsigned int y, char* name, IplImage* roi)
{
	static char lastch = 0;
	static IplImage* dst[256];
	double f;

	if((lastch == name[0]) && (dst[name[0]] != NULL))
	{
		f = ComputeFeatureRoiSpan(dst[name[0]], x, y, name+1, roi);
	}
	else
	{
		if(dst[name[0]] == NULL)
		{
			dst[name[0]] = cvCreateImage(cvSize(src->width, src->height), IPL_DEPTH_8U, 1);
			if(dst[name[0]] == NULL)
			{
				for(int k = 0; k < 256; k++) 
				{
					cvReleaseImage(&(dst[k]));
					dst[k] = NULL;
				}
				dst[name[0]] = cvCreateImage(cvSize(src->width, src->height), IPL_DEPTH_8U, 1);
			}
			if(dst[name[0]] == NULL) return mznan;
			lastch = name[0];
			mzImageConvert(lastch, src->widthStep, dst[lastch]->widthStep, src->width, src->height, src->imageData, (uint8_t *)dst[lastch]->imageData);
		}
		lastch = name[0];
		validRoiChannelMethod = false;
	//	mzImageConvert(lastch, src->widthStep, dst[lastch]->widthStep, src->width, src->height, src->imageData, (uint8_t *)dst[lastch]->imageData);
		f = ComputeFeatureRoiSpan(dst[lastch], x, y, name+1, roi);
	}
//	cvReleaseImage(&dst);
	return f;
}
double ComputeFeature(IplImage* image, const unsigned int x, const unsigned int y, char* name)
{
	static char lastn[4] = "";
	static unsigned int lastr = 0;
	static IplImage* roi = NULL;
	double f = mznan;
	if((strncmp(lastn, name, 3) == 0) && (roi != NULL))
	{
		f = ComputeFeatureRoi(image, x-lastr, y-lastr, name+3, roi);
	}
	else
	{
		if(roi != NULL)
		{
			cvReleaseImage(&roi);
			roi = NULL;
			validRoiChannelMethod = false;
		}
		switch(name[0])
		{
		case 'C':
			{
				strncpy(lastn, name, 3);
				lastn[4] = 0;
				lastr = atoi(lastn+1);
				validRoiChannelMethod = false;
				roi = cvCreateImage(cvSize(2*lastr+1, 2*lastr+1), IPL_DEPTH_8U, 1);
				cvZero(roi);
				cvCircle(roi, cvPoint(lastr, lastr), lastr+1, cvScalar(255, 255, 255));
				cvFloodFill(roi, cvPoint(lastr, lastr), cvScalar(255, 255, 255));
				f = ComputeFeatureRoi(image, x-lastr, y-lastr, name+3, roi);
			}
			break;
		case 'S':
			{
				strncpy(lastn, name, 3);
				lastn[4] = 0;
				lastr = atoi(lastn+1);
				validRoiChannelMethod = false;
				roi = cvCreateImage(cvSize(2*lastr+1, 2*lastr+1), IPL_DEPTH_8U, 1);
				cvZero(roi);
				cvFloodFill(roi, cvPoint(lastr, lastr), cvScalar(255, 255, 255));
				f = ComputeFeatureRoi(image, x-lastr, y-lastr, name+3, roi);
			}
			break;
		default: return mznan;
		}
	}
//	cvReleaseImage(&roi);
	return f;
}

void Info(void)
{
	printf("MazdaCl - computation of image color and texture features\r\n");
	printf("Copyright 1998-2011 by Piotr M. Szczypinski\r\n");  
	printf("Email: pms@p.lodz.pl\r\n\r\n");

	printf("Use: MazdaCl -S image.bmp points.bmp features.txt report.csv\r\n");
	printf(" or: MazdaCl -M image.bmp prefix features.txt\r\n");
	printf(" or: MazdaCl -R image.bmp roi16.bmp features.txt report.csv\r\n\r\n");
	printf(" or: MazdaCl -P image.bmp mask.bmp features.txt report.csv class_name\r\n\r\n");
	printf("     image - [in] color image, input for texture computation\r\n");
	printf("     points - [in] color image with color dots pointing locations for computation\r\n");
	printf("     roi - [in] image with regions of interest drawn with the specified colors\r\n");
	printf("     report - [out] name of a file for writing computation result\r\n");
	printf("     prefix - [in] file name prefix for map files\r\n");
	printf("     features - [in] text file with specified feature names\r\n\r\n");

	printf("       Example of features file content\r\n");
	printf("       		BHHPerc90\r\n");
	printf("       		SXHMean\r\n");
	printf("       		BQD5CZ1Contrast\r\n");
	printf("       		BBD5CH5InvDfMom\r\n\r\n");

	printf("       Recognized colors of roi file (GBR sequence)\r\n");
	printf("       		0x000000FF, 0x0000FF00, 0x00FF0000, 0x00FFFF00,\r\n"); 
	printf("       		0x00FF00FF, 0x0000FFFF, 0x000080FF, 0x008000FF,\r\n"); 
	printf("       		0x0000FF80, 0x0080FF00, 0x00FF0080, 0x00FF8000,\r\n"); 
	printf("       		0x0000C4FF, 0x0000FFC4, 0x00FF00C4, 0x00C4FF00\r\n\r\n"); 
};

struct BoundRect
{
	int l, r, t, b;
};

int Main_R(int argc, char* argv[])
{
	int x = 0;
	int y = 0;
	int i;
	FILE* files;
	FILE* filer;
	IplImage* src = NULL;
	IplImage* dst = NULL;
	IplImage* rois = NULL;

	char linia[MAX_LINE];
	char* token;
	BoundRect bound[MAX_ROIS];


	if( (src = cvLoadImage(argv[2], CV_LOAD_IMAGE_COLOR)) == 0 ) 
	{
		printf("Error loading image: %s\r\n", argv[2]);
		return -52;
	}
	if( (rois = cvLoadImage(argv[3], CV_LOAD_IMAGE_COLOR)) == 0 ) 
	{
		printf("Error loading rois: %s\r\n", argv[3]);
		return -53;
	}
	if((files = fopen(argv[4], "rb")) == 0) 
	{
		printf("Error opening specified features: %s\r\n", argv[4]);
		return -51;
	}
	if((filer = fopen(argv[5], "wb")) == 0) 
	{
		printf("Error opening report: %s\r\n", argv[5]);
		return -54;
	}
	printf("Specified features: %s\r\n", argv[4]);
	printf("Image: %s\r\n", argv[2]);
	printf("Roi: %s\r\n", argv[3]);
	printf("Output: %s\r\n", argv[5]);
	dst = cvCloneImage(src);

	//printf("The specified feature names:\r\n");

	do{
		if(fgets(linia, MAX_LINE-1, files) != NULL)
		{
			linia[MAX_LINE-1] = 0;
			token = strtok(linia, separator);
			if(token != NULL)
			{
				fprintf(filer, "%s,", token);
				//printf("\t%s\r\n", token);
			}
		}
		else token = NULL;
	}while(token!=NULL);

	fprintf(filer, "Category\r\n");
	//printf("\r\n");

	for(i = 0; i < MAX_ROIS; i++)
	{
		bound[i].r = -2;
		bound[i].b = -2;
		bound[i].l = -1;
		bound[i].t = -1;
	}

	for(y = 0; y < rois->height; y++)
	{
		for(x = 0; x < rois->width; x++)
		{
			int rrr = 0;
			CvScalar ggg = cvGet2D(rois, y, x);
			for(i = 0; i < MAX_ROIS; i++)
			{
				if((((RoiColors[i])&0xFF) == ggg.val[2]) &&
					(((RoiColors[i]>>8)&0xFF) == ggg.val[1]) && 	
					(((RoiColors[i]>>16)&0xFF) == ggg.val[0]))
				{
					if(bound[i].r < x) bound[i].r = x;
					if(bound[i].b < y) bound[i].b = y;
					if((unsigned int)bound[i].l > (unsigned int)x) bound[i].l = x;
					if((unsigned int)bound[i].t > (unsigned int)y) bound[i].t = y;
				}
			}
		}
	}

	for(i = 0; i < MAX_ROIS; i++)
	{
		if((bound[i].r - bound[i].l >= 0) && (bound[i].b - bound[i].t >= 0))
		{
			IplImage* roi = cvCreateImage(cvSize(bound[i].r - bound[i].l + 1, bound[i].b - bound[i].t + 1), IPL_DEPTH_8U, 1);

			for(y = bound[i].t; y <= bound[i].b; y++)
			{
				for(x = bound[i].l; x <= bound[i].r; x++)
				{
					CvScalar ggg = cvGet2D(rois, y, x);
					if((((RoiColors[i])&0xFF) == ggg.val[2]) &&
						(((RoiColors[i]>>8)&0xFF) == ggg.val[1]) && 	
						(((RoiColors[i]>>16)&0xFF) == ggg.val[0]))
					{
						cvSet2D(roi, y-bound[i].t, x-bound[i].l, cvScalar(255, 255, 255));
					}
					else
					{
						cvSet2D(roi, y-bound[i].t, x-bound[i].l, cvScalar(0, 0, 0));
					}
				}
			}
			rewind(files);
			validRoiChannelMethod = false;
			GeomFeaturesInvalidate();

			do{
				if(fgets(linia, MAX_LINE-1, files) != NULL)
				{
					linia[MAX_LINE-1] = 0;
					token = strtok(linia, separator);
					if(token != NULL)
					{
						double feat;
						if(strncmp(token, "Geo", 3) == 0) feat = ComputeGeomFeature(bound[i].l, bound[i].t, token, roi);
						else feat = ComputeFeatureRoi(src, bound[i].l, bound[i].t, token, roi);
						if(feat == mznan) fprintf(filer, "NaN,");
						else fprintf(filer, "%.8g,", (float)feat);
					}
				}
				else token = NULL;
			}while(token!=NULL);
			fprintf(filer, "%i\r\n", i);
			cvReleaseImage(&roi);
			printf(".");	
// Bo Mac sobie nie daje rady z wypisaniem kropek ;-)
			fflush(stdout);
		}	
	}
	fclose(filer);
	fclose(files);
	cvReleaseImage(&src);
	cvReleaseImage(&dst);
	cvReleaseImage(&rois);

	printf("\r\nCompleted!\r\n");
	return 0;

//	printf("!!! (-R) Not yet implemented\r\n");
//	return -50;
}



int Main_P(int argc, char* argv[], bool append)
{
	int x = 0;
	int y = 0;
	//int i;
	FILE* files;
	FILE* filer;
	IplImage* src = NULL;
	IplImage* dst = NULL;
	IplImage* rois = NULL;

	char linia[MAX_LINE];
	char* token;
	BoundRect bound;

	if( (src = cvLoadImage(argv[2], CV_LOAD_IMAGE_COLOR)) == 0 ) 
	{
		printf("Error loading image: %s\r\n", argv[2]);
		return -62;
	}
	if( (rois = cvLoadImage(argv[3], CV_LOAD_IMAGE_GRAYSCALE)) == 0 ) 
	{
		printf("Error loading rois: %s\r\n", argv[3]);
		return -63;
	}
	if((files = fopen(argv[4], "rb")) == 0) 
	{
		printf("Error opening specified features: %s\r\n", argv[4]);
		return -61;
	}
	if(append)
	{
		if((filer = fopen(argv[5], "ab")) == 0) 
		{
			printf("Error opening report: %s\r\n", argv[5]);
			return -64;
		}
	}
	else 
	{
		if((filer = fopen(argv[5], "wb")) == 0) 
		{
			printf("Error opening report: %s\r\n", argv[5]);
			return -67;
		}
	}
	printf("Specified features: %s\r\n", argv[4]);
	printf("Image: %s\r\n", argv[2]);
	printf("Roi: %s\r\n", argv[3]);
	if(append) printf("Appending to: %s\r\n", argv[5]);
	else printf("New output: %s\r\n", argv[5]);
	printf("Class: %s\r\n", argv[6]);
	dst = cvCloneImage(src);

	//printf("The specified feature names:\r\n");

	if(!append)
	{
		do{
			if(fgets(linia, MAX_LINE-1, files) != NULL)
			{
				linia[MAX_LINE-1] = 0;
				token = strtok(linia, separator);
				if(token != NULL)
				{
					fprintf(filer, "%s,", token);
					//printf("\t%s\r\n", token);
				}
			}
			else token = NULL;
		}while(token!=NULL);

		fprintf(filer, "Category\r\n");
		//printf("\r\n");
	}
	bound.r = -2;
	bound.b = -2;
	bound.l = -1;
	bound.t = -1;

	for(y = 0; y < rois->height; y++)
	{
		for(x = 0; x < rois->width; x++)
		{
			int rrr = 0;
			CvScalar ggg = cvGet2D(rois, y, x);
			if(ggg.val[0] > 127)
			{
				if(bound.r < x) bound.r = x;
				if(bound.b < y) bound.b = y;
				if((unsigned int)bound.l > (unsigned int)x) bound.l = x;
				if((unsigned int)bound.t > (unsigned int)y) bound.t = y;
			}
		}
	}


	if((bound.r - bound.l >= 0) && (bound.b - bound.t >= 0))
	{
		IplImage* roi = cvCreateImage(cvSize(bound.r - bound.l + 1, bound.b - bound.t + 1), IPL_DEPTH_8U, 1);
		
		//cvThreshold(rois, roi, 127.0, 255.0, CV_THRESH_BINARY);
		for(y = bound.t; y <= bound.b; y++)
		{
			for(x = bound.l; x <= bound.r; x++)
			{
				CvScalar ggg = cvGet2D(rois, y, x);
				if(ggg.val[0] > 127)
				{
					cvSet2D(roi, y-bound.t, x-bound.l, cvScalar(255, 255, 255));
				}
				else
				{
					cvSet2D(roi, y-bound.t, x-bound.l, cvScalar(0, 0, 0));
				}
			}
		}
		rewind(files);
		validRoiChannelMethod = false;
		GeomFeaturesInvalidate();

		do{
			if(fgets(linia, MAX_LINE-1, files) != NULL)
			{
				linia[MAX_LINE-1] = 0;
				token = strtok(linia, separator);
				if(token != NULL)
				{
					double feat;
					if(strncmp(token, "Geo", 3) == 0) feat = ComputeGeomFeature(bound.l, bound.t, token, roi);
					else feat = ComputeFeatureRoi(src, bound.l, bound.t, token, roi);
					if(feat == mznan) fprintf(filer, "NaN,");
					else fprintf(filer, "%.8g,", (float)feat);
				}
			}
			else token = NULL;
		}while(token!=NULL);
		fprintf(filer, "%s\r\n", argv[6]);
		cvReleaseImage(&roi);
	}	

	fclose(filer);
	fclose(files);
	cvReleaseImage(&src);
	cvReleaseImage(&dst);
	cvReleaseImage(&rois);

	printf("\r\nCompleted!\r\n");
	return 0;
}


#define WRITE_FIELD(fi) fwrite(&(fi), sizeof(fi), 1, file);

inline void WriteBmfHeader(FILE* file, int width, int height)
{
	MZ_BITMAPINFOHEADER bih;
	MZ_BITMAPFILEHEADER bfh;
	bih.biSize=sizeof(bih);
	bih.biWidth=width;
	bih.biHeight=height;
	bih.biPlanes=1;
	bih.biBitCount=8*sizeof(double);
	bih.biCompression=0;
	bih.biSizeImage=bih.biWidth*bih.biHeight*sizeof(double);
	bih.biXPelsPerMeter=1000;
	bih.biYPelsPerMeter=1000;
	bih.biClrUsed=0;
	bih.biClrImportant=0;

	bfh.bfType=0x4d42;
	bfh.bfReserved1=0;
	bfh.bfReserved2=0;
	bfh.bfOffBits=sizeof(bfh)+sizeof(bih);
	bfh.bfSize=bfh.bfOffBits+bih.biSizeImage;

	WRITE_FIELD(bfh.bfType);
    WRITE_FIELD(bfh.bfSize);
    WRITE_FIELD(bfh.bfReserved1);
    WRITE_FIELD(bfh.bfReserved2);
    WRITE_FIELD(bfh.bfOffBits);

    WRITE_FIELD(bih.biSize);
    WRITE_FIELD(bih.biWidth);
    WRITE_FIELD(bih.biHeight);
    WRITE_FIELD(bih.biPlanes);
    WRITE_FIELD(bih.biBitCount);
    WRITE_FIELD(bih.biCompression);
    WRITE_FIELD(bih.biSizeImage);
    WRITE_FIELD(bih.biXPelsPerMeter);
    WRITE_FIELD(bih.biYPelsPerMeter);
    WRITE_FIELD(bih.biClrUsed);
    WRITE_FIELD(bih.biClrImportant);

//	fwrite(&bfh, sizeof(bfh), 1, file);
//	fwrite(&bih, sizeof(bih), 1, file);
};
inline void WriteBmfValue(FILE* file, double val)
{
	fwrite(&val, sizeof(val), 1, file);
};

// MazdaCl -M input_image.bmp prefix specified_features.txt
int Main_M(int argc, char* argv[])
{
	int x = 0;
	int y = 0;
	FILE* files;
	FILE* filebmf;
	IplImage* src = 0;
	char linia[MAX_LINE];
	char* token;

	if( (src = cvLoadImage(argv[2], CV_LOAD_IMAGE_COLOR)) == 0 ) 
	{
		printf("Error loading image: %s\r\n", argv[2]);
		return -42;
	}
	if((files = fopen(argv[4], "rb")) == 0) 
	{
		printf("Error opening specified features: %s\r\n", argv[4]);
		return -41;
	}

/*
	printf("Specified features: %s\r\n", argv[4]);
	printf("Image: %s\r\n", argv[2]);
	printf("Output: %s\r\n", argv[3]);
*/
	do{
		if(fgets(linia, MAX_LINE-1, files) != NULL)
		{
			token = strtok(linia, separator);
			if(token != NULL)
			{
				char name[2*MAX_LINE];
				printf("\r\n\t%s\r\n", token);
				sprintf(name, "%s%s.bmf", argv[3], token);
				filebmf = fopen(name, "wb");
				if(filebmf != NULL)
				{
					WriteBmfHeader(filebmf, src->width, src->height);
					for(y = 0; y < src->height; y++)
					{
						for(x = 0; x < src->width; x++)
						{
							validRoiChannelMethod = false;
							double feat = ComputeFeature(src, x, y, token);
							WriteBmfValue(filebmf, feat);
						}
						printf("-");
// Bo Mac sobie nie daje rady z wypisaniem kropek i kresek			
						fflush(stdout);
					}
					fclose(filebmf);
					filebmf = NULL;
				}
				else
				{
					printf("Error: Cannot create file %s\r\n", name);
					return -45;
				}
			}
		}
		else token = NULL;
	}while(token != NULL);

	fclose(files);
	cvReleaseImage(&src);
	printf("\r\nCompleted!\r\n");
	return 0;
}

// MazdaCl -S input_image.bmp points_image.bmp specified_features.txt report.csv
int Main_S(int argc, char* argv[])
{
	int x = 0;
	int y = 0;
	FILE* files;
	FILE* filer;
	IplImage* src = 0;
	IplImage* dst = 0;
	IplImage* points = 0;
	char linia[MAX_LINE];
	char* token;

	if( (src = cvLoadImage(argv[2], CV_LOAD_IMAGE_COLOR)) == 0 ) 
	{
		printf("Error loading image: %s\r\n", argv[2]);
		return -22;
	}
	if( (points = cvLoadImage(argv[3], CV_LOAD_IMAGE_COLOR)) == 0 ) 
	{
		printf("Error loading roi: %s\r\n", argv[3]);
		return -23;
	}
	if((files = fopen(argv[4], "rb")) == 0) 
	{
		printf("Error opening specified features: %s\r\n", argv[4]);
		return -21;
	}
	if((filer = fopen(argv[5], "wb")) == 0) 
	{
		printf("Error opening report: %s\r\n", argv[5]);
		return -24;
	}
	printf("Specified features: %s\r\n", argv[4]);
	printf("Image: %s\r\n", argv[2]);
	printf("RoiPoints: %s\r\n", argv[3]);
	printf("Output: %s\r\n", argv[5]);
	dst = cvCloneImage(src);

	//printf("The specified feature names:\r\n");

	do{
		if(fgets(linia, MAX_LINE-1, files) != NULL)
		{
			linia[MAX_LINE-1] = 0;
			token = strtok(linia, separator);
			if(token != NULL)
			{
				fprintf(filer, "%s,", token);
				//printf("\t%s\r\n", token);
			}
		}
		else token = NULL;
	}while(token!=NULL);

	fprintf(filer, "Category\r\n");
	//printf("\r\n");

	for(y = 0; y < points->height; y++)
	{
		for(x = 0; x < points->width; x++)
		{
			int rrr = 0;
			CvScalar ggg = cvGet2D(points, y, x);
			for(int i = 0; i < MAX_ROIS; i++)
			{
				if((((RoiColors[i])&0xFF) == ggg.val[2]) &&
					(((RoiColors[i]>>8)&0xFF) == ggg.val[1]) && 	
					(((RoiColors[i]>>16)&0xFF) == ggg.val[0]))
				{
					rrr = i+1;
					break;
				}
			}
			if(rrr > 0)
			{
				rewind(files);
				validRoiChannelMethod = false;
				do{
					if(fgets(linia, MAX_LINE-1, files) != NULL)
					{
						linia[MAX_LINE-1] = 0;
						token = strtok(linia, separator);
						if(token != NULL)
						{
							double feat = ComputeFeature(src, x, y, token);
							if(feat == mznan) fprintf(filer, "NaN,");
							else fprintf(filer, "%.8g,", (float)feat);
						}
					}
					else token = NULL;
				}while(token!=NULL);

				fprintf(filer, "%i\r\n", rrr);
				printf(".");	

// Bo Mac sobie nie daje rady z wypisaniem kropek				
				fflush(stdout);
			}
		}
	}
	fclose(filer);
	fclose(files);
	cvReleaseImage(&src);
	cvReleaseImage(&dst);
	cvReleaseImage(&points);

	printf("\r\nCompleted!\r\n");
	return 0;
}



int main(int argc, char* argv[])
{
	if(argc < 5)
	{
		Info();
		return -1;
	}
	else if(strcmp(argv[1], "-S") == 0) 
	{
		if(argc != 6)
		{
			Info();
			return -12;
		}
		else
		{
			return Main_S(argc, argv);
		}
	}
	else if(strcmp(argv[1], "-M") == 0) 
	{
		if(argc != 5)
		{
			Info();
			return -13;
		}
		else
		{
			return Main_M(argc, argv);
		}
	}
	else if(strcmp(argv[1], "-R") == 0) 
	{
		if(argc != 6)
		{
			Info();
			return -14;
		}
		else
		{
			return Main_R(argc, argv);
		}
	}

	else if(strcmp(argv[1], "-Pa") == 0) 
	{
		if(argc != 7)
		{
			Info();
			return -15;
		}
		else
		{
			return Main_P(argc, argv, true);
		}
	}
	else if(strcmp(argv[1], "-P") == 0) 
	{
		if(argc != 7)
		{
			Info();
			return -16;
		}
		else
		{
			return Main_P(argc, argv, false);
		}
	}
	else
	{
		Info();
		return -2;
	}
};
