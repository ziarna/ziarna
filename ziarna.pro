TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += ziarna.cpp

unix|win32: LIBS += -lopencv_core
unix|win32: LIBS += -lopencv_imgproc
unix|win32: LIBS += -lopencv_highgui
unix|win32: LIBS += -lopencv_legacy
