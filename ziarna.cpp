/*
* Ziarna - barley grain image segmentation and orientation analysis
*
* Copyright 2011-2013 Piotr M. Szczypi?ski <piotr.szczypinski@p.lodz.pl>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/


//#include "stdafx.h"
#define _USE_MATH_DEFINES
#include <math.h>
//#include <windows.h>
#include <stdio.h>

#include <opencv2/imgproc/imgproc_c.h>
#include <opencv2/highgui/highgui_c.h>

int SEG_RADIUS = 63; 
int SEG_THRESH = 2;
int SEG_MIN_AREA = 2000;
int SEG_MAX_AREA = 20000;
int SEG_MORPHO = 7;

int TEX_RL_DEPTH = 8;
int TEX_RL_THRESH = 4;
int TEX_MIN_AREA = 100;
int TEX_MAX_AREA = 20000;
int TEX_MORPHO = 7;
int TEX_EXPAND = 0;
int TEX_PLAIN = 1;

int CRE_BLUR = 11;
int CRE_RADIUS = 10;
int CRE_THRESH = 5;
int CRE_LENGTH = 80;

int ELI_HEIGHT = 90;
int ELI_WIDTH = 35;

int SAV_SIDE = 0; // 0obie 1gora 2dol
int SAV_PART = 0; // 0ziarniak 1tekstura 2bruzdka
int SAV_ASSEMBLE = 1; // 0ziarniak 1tekstura 2bruzdka


const CvScalar RoiColors[16] =
{
	{0.0, 0.0, 255.0, 0.0}, 
	{0.0, 255.0, 0.0, 0.0},
	{255.0, 0.0, 0.0, 0.0},
	{255.0, 255.0, 0.0, 0.0}, 
	{255.0, 0.0, 255.0, 0.0}, 
	{0.0, 255.0, 255.0, 0.0}, 
	{0.0, 128.0, 255.0, 0.0}, 
	{128.0, 0.0, 255.0, 0.0}, 
	{0.0, 255.0, 128.0, 0.0}, 
	{128.0, 255.0, 0.0, 0.0}, 
	{255.0, 0.0, 128.0, 0.0}, 
	{255.0, 128.0, 0.0, 0.0}, 
	{0.0, 196.0, 255.0, 0.0}, 
	{0.0, 255.0, 196.0, 0.0},
	{255.0, 0.0, 196.0, 0.0}, 
	{196.0, 255.0, 0.0, 0.0} 
};
//  clRed, clLime, clBlue, clAqua, clFuchsia, clYellow,
//	0x0080FF, 0x8000FF, 0x00FF80, 0x80FF00, 0xFF0080, 
//	0xFF8000, 0x00C4FF, 0x00FFC4, 0xFF00C4, 0xC4FF00



void TransformRLVert(IplImage* src, IplImage* dst, float depth)
{
	for(int y = 0; y < src->height; y++)
	for(int x = 0; x < src->width; x++)
	{
		CvScalar g = cvGet2D(src, y, x);
		int length = 1;
		int yy;
		for(yy = y+1; yy < src->height; yy++)
		{
			CvScalar gg = cvGet2D(src, yy, x);
			if(abs(g.val[0]-gg.val[0])<depth) length++;
			else break;
		}
		for(yy = y-1; yy >=0; yy--)
		{
			CvScalar gg = cvGet2D(src, yy, x);
			if(abs(g.val[0]-gg.val[0])<depth) length++;
			else break;
		}
		cvSet2D(dst, y, x, cvScalar(length, length, length));
	}
}

void SegmentTexture(IplImage* buf)
{
	IplImage* buf2 = cvCreateImage(cvSize(buf->width, buf->height), IPL_DEPTH_8U, 1);
	cvSmooth(buf, buf2, CV_MEDIAN, TEX_MORPHO);
	cvThreshold(buf2, buf, TEX_RL_THRESH, 255.0, CV_THRESH_BINARY_INV);
	IplConvKernel* element7 = 0;
    element7 = cvCreateStructuringElementEx(TEX_MORPHO, TEX_MORPHO, TEX_MORPHO/2, TEX_MORPHO/2, CV_SHAPE_ELLIPSE);
	cvErode(buf, buf2, element7, 1);
    cvDilate(buf2, buf, element7, 1);
	cvDilate(buf, buf2, element7, 1);
    cvErode(buf2, buf, element7, 1);
	cvReleaseStructuringElement(&element7);
	cvReleaseImage(&buf2);

	for(int yd = 0; yd < buf->height; yd++)
	{
		for(int xd = 0; xd < buf->width; xd++)
		{
			CvConnectedComp cop;
			CvScalar gd = cvGet2D(buf, yd, xd);
			if(gd.val[0] > 127.0)
			{
				cvFloodFill(buf, cvPoint(xd, yd), cvScalar(255.0, 0.0, 0.0), cvScalarAll(0), cvScalarAll(0), &cop); // int flags=4, CvArr* mask=NULL)
				if(cop.area < TEX_MIN_AREA || cop.area > TEX_MAX_AREA)
				{
					cvFloodFill(buf, cvPoint(xd, yd), cvScalar(0.0, 0.0, 0.0), cvScalarAll(0), cvScalarAll(0), &cop); // int flags=4, CvArr* mask=NULL)
					break;
				}
			}
		}
	}
}

/*
void SegmentTexture(IplImage* buf, float threshold)
{
	IplImage* buf2 = cvCreateImage(cvSize(buf->width, buf->height), IPL_DEPTH_8U, 1);
	cvSmooth(buf, buf2, CV_MEDIAN, 7);
	cvThreshold(buf2, buf, threshold, 255.0, CV_THRESH_BINARY_INV);
	IplConvKernel* element7 = 0;
    element7 = cvCreateStructuringElementEx( 7, 7, 3, 3, CV_SHAPE_ELLIPSE);
	IplConvKernel* element3 = 0;
    element3 = cvCreateStructuringElementEx( 3, 3, 1, 1, CV_SHAPE_ELLIPSE);
	cvErode(buf, buf2, element7, 1);
    cvDilate(buf2, buf, element7, 1);
	cvDilate(buf, buf2, element7, 1);
    cvErode(buf2, buf, element7, 1);
	cvReleaseStructuringElement(&element3);	
	cvReleaseStructuringElement(&element7);
	cvReleaseImage(&buf2);
}*/

void ExpandTexture(IplImage* buf, IplImage* msk)
{
	for(int yd = 1; yd < buf->height-1; yd++)
	{
		for(int xd = 1; xd < buf->width-1; xd++)
		{
			if(cvGet2D(msk, yd, xd).val[0] < 255.0)
			{
				if(cvGet2D(buf, yd-1, xd).val[0] > 64.0 || 
					cvGet2D(buf, yd, xd).val[0] > 64.0 ||
					cvGet2D(buf, yd-1, xd-1).val[0] > 64.0 ||
					cvGet2D(buf, yd-1, xd+1).val[0] > 64.0)
					cvSet2D(buf, yd, xd, cvScalar(255.0, 255.0, 255.0));
			}
		}
	}
/*
	cvShowImage("Wej",msk);
	cvWaitKey( 1000 );
	cvWaitKey( 1000 );
	cvShowImage("Wej",buf);
	cvWaitKey( 1000 );
	cvWaitKey( 1000 );
*/
}

void TeksturaZiarniaka(IplImage* img, IplImage* msk, IplImage* dst, CvBox2D box)
{
	IplImage* bf4 = cvCreateImage(cvSize(box.size.width*1.2, box.size.height*1.2), IPL_DEPTH_8U, 1);
	IplImage* bf5 = cvCreateImage(cvSize(box.size.width*1.2, box.size.height*1.2), IPL_DEPTH_8U, 1);
	IplImage* bf6 = cvCreateImage(cvSize(box.size.width*1.2, box.size.height*1.2), IPL_DEPTH_8U, 1);
	//cvZero(dst);

	float cosa = cos(box.angle*M_PI/180.0);
	float sina = sin(box.angle*M_PI/180.0);
	CvMat* mat = cvCreateMat(2, 3, CV_64FC1);
	cvSet2D(mat, 0, 0, cvScalar(cosa));
	cvSet2D(mat, 0, 1, cvScalar(-sina));
	cvSet2D(mat, 1, 0, cvScalar(sina));
	cvSet2D(mat, 1, 1, cvScalar(cosa));
	cvSet2D(mat, 0, 2, cvScalar(box.center.x));
	cvSet2D(mat, 1, 2, cvScalar(box.center.y));

	cvGetQuadrangleSubPix(img, bf4, mat);
	TransformRLVert(bf4, bf5, TEX_RL_DEPTH);
	cvGetQuadrangleSubPix(msk, bf4, mat);
	cvSmooth(bf4, bf6, CV_GAUSSIAN, 5, 15);
	cvAdd(bf6, bf5, bf4);
	SegmentTexture(bf4);
	if(TEX_EXPAND) 
	{
		cvGetQuadrangleSubPix(msk, bf5, mat);
		ExpandTexture(bf4, bf5);
	}

	if(TEX_PLAIN)
	{
		cvGetQuadrangleSubPix(msk, bf5, mat);
		cvAdd(bf4, bf5, bf6);
		cvThreshold(bf6, bf4, 127.0, 255.0, CV_THRESH_BINARY_INV);
	}

	float xp = -cosa * box.size.width  + sina * box.size.height;
	float yp = -cosa * box.size.height - sina * box.size.width;
	xp *= 0.6;
	yp *= 0.6;
	xp += box.center.x;
	yp += box.center.y;

	cvSet2D(mat, 0, 0, cvScalar(cosa));
	cvSet2D(mat, 0, 1, cvScalar(-sina));
	cvSet2D(mat, 1, 0, cvScalar(sina));
	cvSet2D(mat, 1, 1, cvScalar(cosa));
	cvSet2D(mat, 0, 2, cvScalar(xp));
	cvSet2D(mat, 1, 2, cvScalar(yp));
	//cvGetQuadrangleSubPix(bf4, dst, mat);
	cvWarpAffine(bf4, dst, mat, CV_INTER_LINEAR);
	cvReleaseMat(&mat);
	cvReleaseImage(&bf4);
	cvReleaseImage(&bf5);
	cvReleaseImage(&bf6);
	
}



int OrientacjaZiarniaka(IplImage* bf3, IplImage* bf2, CvBox2D box)
{
	IplImage* bf4 = cvCreateImage(cvSize(32, 128), IPL_DEPTH_8U, 1);
	IplImage* bf5 = cvCreateImage(cvSize(32, 128), IPL_DEPTH_8U, 1);
	IplImage* bf1 = cvCreateImage(cvSize(bf2->width, bf2->height), IPL_DEPTH_8U, 1);
	cvZero(bf1);

	cvEllipseBox(bf1, box, cvScalar(127.0,0.0,0.0), 1);
	cvEllipseBox(bf2, box, cvScalar(127.0,0.0,0.0), 1);
	float cosa = cos(box.angle*M_PI/180.0);
	float sina = sin(box.angle*M_PI/180.0);
	float x = box.size.width/2.0+1.0;
	float y = box.size.height*0.375;
	float y1 = cosa*y + sina*x + box.center.y;
	float x1 = cosa*x - sina*y + box.center.x;
	float y2 = -cosa*y + sina*x + box.center.y;
	float x2 = cosa*x + sina*y + box.center.x;
	float y3 = cosa*y - sina*x + box.center.y;
	float x3 = -cosa*x - sina*y + box.center.x;
	float y4 = -cosa*y - sina*x + box.center.y;
	float x4 = - cosa*x + sina*y + box.center.x;
	cvLine(bf1, cvPoint(x2, y2), cvPoint(x4, y4), cvScalar(127.0,0.0,0.0));
	cvLine(bf1, cvPoint(x3, y3), cvPoint(x1, y1), cvScalar(127.0,0.0,0.0));
	cvLine(bf2, cvPoint(x2, y2), cvPoint(x4, y4), cvScalar(127.0,0.0,0.0));
	cvLine(bf2, cvPoint(x3, y3), cvPoint(x1, y1), cvScalar(127.0,0.0,0.0));
	y = box.size.height*0.125;
	y1 = cosa*y + sina*x + box.center.y;
	x1 = cosa*x - sina*y + box.center.x;
	y2 = -cosa*y + sina*x + box.center.y;
	x2 = cosa*x + sina*y + box.center.x;
	y3 = cosa*y - sina*x + box.center.y;
	x3 = -cosa*x - sina*y + box.center.x;
	y4 = -cosa*y - sina*x + box.center.y;
	x4 = - cosa*x + sina*y + box.center.x;
	cvLine(bf1, cvPoint(x2, y2), cvPoint(x4, y4), cvScalar(127.0,0.0,0.0));
	cvLine(bf1, cvPoint(x3, y3), cvPoint(x1, y1), cvScalar(127.0,0.0,0.0));
	cvLine(bf2, cvPoint(x2, y2), cvPoint(x4, y4), cvScalar(127.0,0.0,0.0));
	cvLine(bf2, cvPoint(x3, y3), cvPoint(x1, y1), cvScalar(127.0,0.0,0.0));
	y = box.size.height*0.25;
	y1 = cosa*y + box.center.y;
	x1 = -sina*y + box.center.x;
	y2 = -cosa*y + box.center.y;
	x2 = sina*y + box.center.x;
	CvConnectedComp comp1;
	CvConnectedComp comp2;
	cvFloodFill(bf1, cvPoint(x1, y1), cvScalar(192.0,0.0,0.0), cvScalarAll(0), cvScalarAll(0), &comp1);
	cvFloodFill(bf2, cvPoint(x1, y1), cvScalar(192.0,0.0,0.0), cvScalarAll(0), cvScalarAll(0), &comp2);
	float ra1 = comp2.area/comp1.area;
	cvFloodFill(bf1, cvPoint(x2, y2), cvScalar(192.0,0.0,0.0), cvScalarAll(0), cvScalarAll(0), &comp1);
	cvFloodFill(bf2, cvPoint(x2, y2), cvScalar(192.0,0.0,0.0), cvScalarAll(0), cvScalarAll(0), &comp2);
	float ra2 = comp2.area/comp1.area;
	//y = box.size.height*0.5;
	//y1 = cosa*y + box.center.y;
	//x1 = -sina*y + box.center.x;
	//y2 = -cosa*y + box.center.y;
	//x2 = sina*y + box.center.x;

	//y = x = box.size.width/128.0;
	y = x = box.size.height/128.0;
	CvMat* mat = cvCreateMat(2, 3, CV_64FC1);
	if(ra2 > ra1)
	{
		cvSet2D(mat, 0, 0, cvScalar(cosa*x));
		cvSet2D(mat, 0, 1, cvScalar(-sina*x));
		cvSet2D(mat, 1, 0, cvScalar(sina*y));
		cvSet2D(mat, 1, 1, cvScalar(cosa*y));
	}
	else
	{
		cvSet2D(mat, 0, 0, cvScalar(-cosa*x));
		cvSet2D(mat, 0, 1, cvScalar(sina*x));
		cvSet2D(mat, 1, 0, cvScalar(-sina*y));
		cvSet2D(mat, 1, 1, cvScalar(-cosa*y));
	}
	cvSet2D(mat, 0, 2, cvScalar(box.center.x));
	cvSet2D(mat, 1, 2, cvScalar(box.center.y));
	cvGetQuadrangleSubPix(bf3, bf4, mat);

	cvSmooth(bf4, bf5, CV_BLUR, 1, CRE_BLUR);
	cvAdaptiveThreshold(bf5, bf4, 255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, CRE_RADIUS*2+1, CRE_THRESH);

	int bruz = 0;
	for(int yd = 0; yd < bf5->height; yd++)
	{
		for(int xd = 0; xd < bf5->width; xd++)
		{
			CvConnectedComp cop;
			CvScalar gd = cvGet2D(bf4, yd, xd);
			if(gd.val[0] <= 127.0)
			{
				cvFloodFill(bf4, cvPoint(xd, yd), cvScalar(200.0, 0.0, 0.0), cvScalarAll(0), cvScalarAll(0), &cop); // int flags=4, CvArr* mask=NULL)
				if(cop.rect.height > CRE_LENGTH) 
				{
					bruz = 1;
					break;
				}
			}
		}
	}
	cvReleaseMat(&mat);
	cvReleaseImage(&bf1);
	cvReleaseImage(&bf5);
	cvReleaseImage(&bf4);
	if(ra1>ra2) bruz+=2;
	return bruz;
}

//===================================================================================================
//===================================================================================================
//===================================================================================================
//===================================================================================================
//===================================================================================================
//===================================================================================================

#define CZYTAJ(X) if(strcmp(#X, str)==0) X=i;

void Parametry(char* filename)
{
    char linia[256];
    char str[128];
    int i;
    FILE* plik = fopen(filename, "rt");
    while(fgets(linia, 250, plik))
    {
        if(sscanf(linia, "%s %i", str, &i) > 1)
        {
            CZYTAJ(SEG_RADIUS)// 127;
            CZYTAJ(SEG_THRESH)// 2;
            CZYTAJ(SEG_MIN_AREA)// 2000;
            CZYTAJ(SEG_MAX_AREA)// 20000;
            CZYTAJ(SEG_MORPHO)// 7;

            CZYTAJ(TEX_RL_DEPTH)// 8;
            CZYTAJ(TEX_RL_THRESH)// 4;
            CZYTAJ(TEX_MIN_AREA)// 100;
            CZYTAJ(TEX_MAX_AREA)// 20000;
            CZYTAJ(TEX_MORPHO)// 7;
            CZYTAJ(TEX_EXPAND)// 1;
            CZYTAJ(TEX_PLAIN)// 1;

            CZYTAJ(CRE_BLUR)// 11;
            CZYTAJ(CRE_RADIUS)// 21;
            CZYTAJ(CRE_THRESH)// 5;
            CZYTAJ(CRE_LENGTH)// 80;
            CZYTAJ(ELI_HEIGHT)// 90;
            CZYTAJ(ELI_WIDTH)// 35;

            CZYTAJ(SAV_SIDE)// 1;
            CZYTAJ(SAV_PART)// 1;
            CZYTAJ(SAV_ASSEMBLE)// 1;
        }
    }
    fclose(plik);
}

//===================================================================================================
//===================================================================================================
//int _tmain(int argc, _TCHAR* argv[])
int main(int argc, char* argv[])
{
    if(argc != 4)
	{
        printf("Zle wywolanie programu\n");
        printf("Ziarna input_image options_file output_file_prefix\n");
		return -1;
	}
		
	if(SAV_ASSEMBLE <= 0) SAV_ASSEMBLE = 1;
    Parametry(argv[2]);

	int width, height;
	IplImage* src = 0;
	IplImage* bf0 = 0;
	IplImage* bf1 = 0;
	IplImage* bf2 = 0;
	IplImage* bf3 = 0;
	IplImage* roi = 0;
	//IplImage* bf4 = 0;
	//IplImage* bf5 = 0;
	CvMemStorage* storage;
	CvSeq* contours;
	CvMemStorage* storage2;
	CvSeq* contours2;

	IplConvKernel* element7 = 0;
    element7 = cvCreateStructuringElementEx( 7, 7, 3, 3, CV_SHAPE_ELLIPSE);
//	IplConvKernel* element3 = 0;
//    element3 = cvCreateStructuringElementEx( 3, 3, 1, 1, CV_SHAPE_ELLIPSE);

	if( (src = cvLoadImage(argv[1],1)) == 0 ) return -1;
	//if( (src = cvLoadImage("Ihf.bmp",1)) == 0 ) return -1;
	width = src->width;
	height = src->height;
	bf0 = cvCreateImage(cvSize(src->width, src->height), IPL_DEPTH_8U, 1);
	bf1 = cvCreateImage(cvSize(src->width, src->height), IPL_DEPTH_8U, 1);
	bf2 = cvCreateImage(cvSize(src->width, src->height), IPL_DEPTH_8U, 1);
	bf3 = cvCreateImage(cvSize(src->width, src->height), IPL_DEPTH_8U, 1);
	roi = cvCreateImage(cvSize(src->width, src->height), IPL_DEPTH_8U, 3);
	//bf4 = cvCreateImage(cvSize(32, 128), IPL_DEPTH_8U, 1);
	//bf5 = cvCreateImage(cvSize(32, 128), IPL_DEPTH_8U, 1);

    cvNamedWindow("Wej",1);
    cvNamedWindow("Wyj",1);
    cvShowImage("Wej",src);
//    cvWaitKey(0);

	cvCvtColor(src, bf1, CV_BGR2GRAY);
	cvCvtColor(src, bf3, CV_BGR2GRAY);
	
	if(SEG_RADIUS<=0) cvThreshold(bf1, bf2, SEG_THRESH, 63.0, CV_THRESH_BINARY);
	else cvAdaptiveThreshold(bf1, bf2, 63, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, SEG_RADIUS*2+1, SEG_THRESH);
	
    cvErode(bf2, bf1, element7, 1);
    cvDilate(bf1, bf2, element7, 1);
    cvDilate(bf2, bf1, element7, 1);
    cvErode(bf1, bf2, element7, 1);

    cvShowImage("Wyj",bf2);
    cvWaitKey(0);

	for(int x = 0; x < width; x++)
	{
		//CvConnectedComp comp;
		CvScalar g = cvGet2D(bf2, height-1, x);
		if(g.val[0] < 250) cvFloodFill(bf2, cvPoint(x, height-1), cvScalar(255.0, 255.0, 255.0)); // int flags=4, CvArr* mask=NULL)
		g = cvGet2D(bf2, 0, x);
		if(g.val[0] < 250) cvFloodFill(bf2, cvPoint(x, 0), cvScalar(255.0, 255.0, 255.0)); // int flags=4, CvArr* mask=NULL)
	}
	for(int y = 0; y < height; y++)
	{
		//CvConnectedComp comp;
		CvScalar g = cvGet2D(bf2, y, width-1);
		if(g.val[0] < 250) cvFloodFill(bf2, cvPoint(width-1, y), cvScalar(255.0, 255.0, 255.0)); // int flags=4, CvArr* mask=NULL)
		g = cvGet2D(bf2, y, 0);
		if(g.val[0] < 250) cvFloodFill(bf2, cvPoint(0, y), cvScalar(255.0, 255.0, 255.0)); // int flags=4, CvArr* mask=NULL)
	}
//	cvShowImage("Wyj",bf2);
//	cvWaitKey( 500 );

	cvThreshold(bf2, bf1, 127.0, 255.0, CV_THRESH_BINARY_INV);
//	cvShowImage("Wyj",bf1);
//	cvWaitKey( 500 );

	int ziaren = 0;
	for(int y = 0; y < height; y++)
	for(int x = 0; x < width; x++)
	{
		CvConnectedComp comp;
		CvScalar g = cvGet2D(bf1, y, x);
		if(g.val[0] >= 250)
		{
			cvFloodFill(bf1, cvPoint(x, y), cvScalar(240.0, 0.0, 0.0), cvScalarAll(0), cvScalarAll(0), &comp); // int flags=4, CvArr* mask=NULL)
			if(comp.area < SEG_MIN_AREA || comp.area > SEG_MAX_AREA) cvFloodFill(bf1, cvPoint(x, y), cvScalar(0.0, 0.0, 0.0));
			else ziaren++;
		}
	}
	printf("Input image: %s\n", argv[1]);
	printf("Parameters: %s\n", argv[2]);
	printf("Grains: %.4i\n\n", ziaren);

	cvCopy(bf1, bf2);
//	cvShowImage("Wyj",bf2);
//	cvWaitKey( 500 );

	storage = cvCreateMemStorage(0);
	contours = 0;
	storage2 = cvCreateMemStorage(0);
	contours2 = 0;
	cvFindContours( bf1, storage, &contours, sizeof(CvContour), CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cvPoint(0,0) );
    //contours = cvApproxPoly( contours, sizeof(CvContour), storage, CV_POLY_APPROX_DP, 3, 1 );
	CvSeq* _contours = contours;
/*
	cvCopy(bf2, bf1);
    cvDrawContours( bf2, _contours, cvScalar(127.0,0.0,0.0), cvScalar(127.0,0.0,0.0), 1, 3, CV_AA, cvPoint(0,0) );
    cvShowImage("Wyj",bf2);
	cvWaitKey( 500 );
*/

	cvDrawContours( src, _contours, cvScalar(127.0,0.0,0.0), cvScalar(127.0,0.0,0.0), 1, 2, CV_AA, cvPoint(0,0) );
//    cvShowImage("Wyj",src);
//	cvWaitKey( 500 );

	//cvCopy(bf2, bf1);
	cvThreshold(bf2, bf1, 127.0, 255.0, CV_THRESH_BINARY_INV);
	cvZero(bf0);


	int roii = 0;
	int roil = 0;
	int asse = 0;
	cvCvtColor(bf1, roi, CV_GRAY2BGR);

	while(_contours != NULL)
	{
//int SAV_TOP = 1;
//int SAV_BOTTOM = 1;
//int SAV_PART = 1; 0 ziarno 1 tekstura 2 bruzdka
		

		CvBox2D box = cvFitEllipse2(_contours);
		
//		cvCopy(bf2, bf1);
/*
	cvShowImage("Wyj",bf3);
	cvWaitKey( 500 );
	cvShowImage("Wyj",bf2);
	cvWaitKey( 500 );
*/
		int orien = OrientacjaZiarniaka(bf3, bf2, box);
		
		if( (SAV_SIDE == 0) ||
			(SAV_SIDE == 1 && (!(orien&1))) || 
			(SAV_SIDE == 2 && (orien&1) )
			)
		{

	/*
			int y = box.size.height*0.5;
			float cosa = cos(box.angle*M_PI/180.0);
			float sina = sin(box.angle*M_PI/180.0);
			int y2 = cosa*y + box.center.y;
			int x2 = -sina*y + box.center.x;
			int y1 = -cosa*y + box.center.y;
			int x1 = sina*y + box.center.x;
			switch(orien)
			{
			case 3:
				cvCircle(src, cvPoint(x2, y2), 2, cvScalar(0.0, 0.0, 127.0), 2);
				break;
			case 2:
				cvCircle(src, cvPoint(x2, y2), 2, cvScalar(0.0, 127.0, 0.0), 2);
				break;
			case 1:
				cvCircle(src, cvPoint(x1, y1), 2, cvScalar(0.0, 0.0, 127.0), 2);
				break;
			default:
				cvCircle(src, cvPoint(x1, y1), 2, cvScalar(0.0, 127.0, 0.0), 2);
				break;
			}
	*/

			if(SAV_PART == 0) 
			{

				cvDrawContours( roi, _contours, RoiColors[roii % 16], cvScalar(0.0,0.0,0.0), 0, CV_FILLED, 8, cvPoint(0,0));
				asse++;
				if(asse>=SAV_ASSEMBLE)
				{
					asse = 0;
					roii++;
				}
			}

			if(SAV_PART == 1) 
			{
				if(!(orien&2))
				{
					box.angle += 180.0;
				}
				cvZero(bf0);
				TeksturaZiarniaka(bf3, bf1, bf0, box);
	//			cvShowImage("Wyj",bf0);
	//			cvWaitKey( 500 );
	//			cvWaitKey( 500 );

				cvFindContours( bf0, storage2, &contours2, sizeof(CvContour), CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cvPoint(0,0));
				cvDrawContours( roi, contours2, RoiColors[roii % 16], cvScalar(0.0,0.0,0.0), 2, CV_FILLED, 8, cvPoint(0,0));
				asse++;
				if(asse>=SAV_ASSEMBLE)
				{
					asse = 0;
					roii++;
				}
			}


			//cvShowImage("Wyj",bf5);
			//cvWaitKey( 500 );
			if(SAV_PART == 2) 
			{
				box.size.height *= ((float)ELI_HEIGHT/100.0);
				box.size.width *= ((float)ELI_WIDTH/100.0);
				cvEllipseBox(roi, box, RoiColors[roii % 16], CV_FILLED);
				asse++;
				if(asse>=SAV_ASSEMBLE)
				{
					asse = 0;
					roii++;
				}
			}
		}
		_contours = _contours->h_next;

//		cvShowImage("Wyj",roi);
//		cvWaitKey( 500 );

		if(((roii > roil) && (roii % 16 == 0)) || ((_contours == NULL)&&((roii > 0) || (asse > 0))))
		{
			roil = roii;
			asse = 0;
			char tekst[1024];
			sprintf(tekst, "%s%.4i.bmp", argv[3], (roii-1)/16);
			printf("%s - %i ROIs\n", tekst, (roii%16>0 ?roii%16 :16));
			cvSaveImage(tekst, roi);
			cvCvtColor(bf1, roi, CV_GRAY2BGR);
		}
			
	}






//	cvFindContours( bf0, storage, &contours, sizeof(CvContour), CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cvPoint(0,0) );
//	cvDrawContours( src, contours, cvScalar(0.0,0.0,127.0), cvScalar(0.0,0.0,127.0), 1, CV_FILLED, 8, cvPoint(0,0) );
//	cvDrawContours( src, contours, cvScalar(0.0,0.0,127.0), cvScalar(0.0,0.0,127.0), 1, 1, CV_AA, cvPoint(0,0) );


//	cvThreshold(bf1, bf2, 127.0, 127.0, CV_THRESH_BINARY_INV);
//	cvThreshold(bf0, bf1, 127.0, 127.0, CV_THRESH_BINARY);
//	cvAdd(bf2, bf1, bf0);
	


	


	//cvShowImage("Wyj",bf0);
	//cvSaveImage("segment.bmp", bf0);
//	cvWaitKey(1000); 
	
//   cvShowImage("Debug",roi);
//	cvSaveImage("wynik.bmp", src);
//	cvWaitKey( 0 );


//	cvShowImage("Wyj",bf1);
	//cvWaitKey( 500 );
//	cvWaitKey( 0 );
	
	return 0;







/*
	IplImage* img = cvLoadImage("image.bmp");
	IplImage* img2 = cvCreateImage(cvSize(img->width, img->height), IPL_DEPTH_8U, 1);
	IplImage* img3 = cvCreateImage(cvSize(img->width, img->height), IPL_DEPTH_8U, 1);
	IplImage* mask = cvCreateImage(cvSize(img->width, img->height), IPL_DEPTH_8U, 1);

	cvCvtColor(img, img2, CV_BGR2GRAY);
	cvNamedWindow("Wynik", 1);
	cvShowImage("Wynik",img2);
	cvWaitKey(500); 

	cvAdaptiveThreshold(img2, img3, 255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 127, 2);
	IplConvKernel* element7 = 0;
    element7 = cvCreateStructuringElementEx( 7, 7, 3, 3, CV_SHAPE_ELLIPSE);
    cvErode(img3, mask, element7, 1);
    cvDilate(mask, img3, element7, 1);
    cvDilate(img3, mask, element7, 1);
    cvErode(mask, img3, element7, 1);
	cvThreshold(img3, mask, 127, 255.0, CV_THRESH_BINARY_INV);
	cvSmooth(mask, img3, CV_GAUSSIAN, 5, 15);
	cvCopy(img3, mask);
	cvShowImage("Wynik", mask);
	cvWaitKey(1000); 

	TransformRLVert(img2, img3, 12);
	SegmentTexture(img3, 4);
	cvShowImage("Wynik",img3);
	cvWaitKey(1000); 

	TransformRLVert(img2, img3, 12);
	cvAdd(mask, img3, img2);
	SegmentTexture(img2, 4);
	cvShowImage("Wynik",img2);

	cvWaitKey(0); 
	return 0;
	*/
}

