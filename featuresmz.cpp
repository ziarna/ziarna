/*****************************************************************/
/*                                                               */
/* MODULE: Features.cpp                                          */
/*                                                               */
/* FUNCTIONALITY:				                                 */
/*   Textural feature computation module                         */
/*   Modul obliczania cech tekstury                              */
/*                                                               */
/* AUTHOR:                                                       */
/*   Piotr M. Szczypinski                                        */
/*   Email: pms@p.lodz.pl                                        */
/*                                                               */
/* Copyright (C) 1998-2011 by Piotr M. Szczypinski               */
/*                                                               */
/*****************************************************************/

#include "featuresmz.h"
#define _USE_MATH_DEFINES 
#include <math.h>
#include <string.h>
#include <stdlib.h>
//#include <malloc.h>
//#include <windows.h>
//windows.h for: uint8_t, 
#include <stdio.h>





//#include <stdlib.h>
//#include <stdio.h>
//#include <windef.h> //def. max(a, b)
//#define max(a, b)  (((a) > (b)) ? (a) : (b))

#define Eps 1E-20
#define Ln10 2.302585092994
#define N_sigma 3

const int Lcom = 12; //*Liczba obliczanych cech com
const int Lhist = 13; //*Liczba cech histogramu
const int Lrlm = 5; //*Liczba cech RunLenght
const int Lgrad = 10; //*Liczba cech gradientowych
const int Larm = 6; //*Liczba cech modelu autoregresji

const char comnames[Lcom][12]=
{{"Area"},    //0
{"AngScMom"},    //1
{"Contrast"},    //2
{"Correlat"},    //3
{"SumOfSqs"},    //4
{"InvDfMom"},    //5
{"SumAverg"},    //6
{"SumVarnc"},    //7
{"SumEntrp"},    //8
{"Entropy"},    //9
{"DifVarnc"},    //10
{"DifEntrp"}};   //11

char histnames[Lhist][12]=
{{"Mean"},    //0
{"Variance"},    //1
{"Skewness"},    //2
{"Kurtosis"},    //3
{"Perc01"},
{"Perc10"},
{"Perc50"},
{"Perc90"},
{"Perc99"},
{"Maxm01"},
{"Domn01"},
{"Maxm10"},
{"Domn10"},
};

char gradnames[Lgrad][16]=
{{"Area"},    //0
{"Mean"},    //1
{"Variance"},    //2
{"Skewness"},    //3
{"Kurtosis"},    //4
{"NonZeros"},
{"Perc01"},
{"Perc10"},
{"Perc50"},
{"Perc90"}
};

char rlmnames[Lrlm][12]=
{{"RLNonUni"},    //0
{"GLevNonU"},    //1
{"LngREmph"},    //2
{"ShrtREmp"},    //3
{"Fraction"},
};

char armnames[Larm][12]=
{{"Area"},  //0
{"Teta1"},    //1
{"Teta2"},    //2
{"Teta3"},    //3
{"Teta4"},    //4
{"Sigma"}};//5

int mzMulDiv(int nNumber, int nNumerator, int nDenominator)
{
	return (int)((nNumber * nNumerator)/nDenominator);
//	return (int)(((long int)nNumber * nNumerator)/nDenominator);
}


//====================================================================================================
// Konwersja obrazu RGB 24 na monochromatyczny 8 bitow.
// colortransform - rodzaj konwersji
// xmax - szerokosc obrazu
// ymax - wysokosc obrazu
// linelength - dlugosc linii poziomej w bajtach
// input - wskaznik do bufora obrazu RGB 24
// output - wskaznik do bufora obrazu monochromatycznego 
void mzImageConvert(char colortransform, int linelengthi, int linelengtho, int width, int height, void* input, uint8_t* output)
{
	for(int y = 0; y < height; y++)
	{
		uint8_t* ptr = (uint8_t*)input + linelengthi * y;
		uint8_t* ptro = (uint8_t*)output + linelengtho * y;
		/* Rownania zamiany RGB na Greyscale wg CCIR Recomendation 601-1 */
		/*       Y = 299/1000*R + 587/1000*G+ 114/1000*B                 */
		for(int x = 0; x < width; x++)
		{
			switch(colortransform)
			{
				//Red
			case 'R': *ptro = (*(ptr+2)); break;
				//Green
			case 'G': *ptro = (*(ptr+1)); break;
				//Blue
			case 'B': *ptro = (*(ptr)); break;
				//U channel  B-Y
			case 'U':
				*ptro = (((int)886**(ptr)-(int)587**(ptr+1)-(int)299**(ptr+2)+886*255)/1772);
				break;
				//V channel  R-Y
			case 'V':
				*ptro = ((-(int)114**(ptr)-(int)587**(ptr+1)+(int)701**(ptr+2)+701*255)/1402);
				break;
				//Phase
			case 'H':
				{
					double u = ((886.0*(double)*(ptr)-587.0*(double)*(ptr+1)-299.0*(double)*(ptr+2))/886.0);
					double v = ((-114.0*(double)*(ptr)-587.0*(double)*(ptr+1)+701.0*(double)*(ptr+2))/701.0);
					if(u == 0 && v == 0) *ptro = 0;
					else
					{
						double h = atan2(v, u);
						if (h<0) h+=(2*M_PI);
						*ptro = (uint8_t)(h*40.58451049);
					}
				}
				break;
				//Saturation
			case 'S':
				{
					double u = ((886.0*(double)*(ptr)-587.0*(double)*(ptr+1)-299.0*(double)*(ptr+2))/886.0);
					double v = ((-114.0*(double)*(ptr)-587.0*(double)*(ptr+1)+701.0*(double)*(ptr+2))/701.0);
					*ptro = (uint8_t)(0.937*sqrt(u*u+v*v));
				}
				break;
				//I
			case 'I':
				*ptro = ((-(int)3213**(ptr)-(int)2744**(ptr+1)+(int)5957**(ptr+2)+1519290)/11916);
				break;
				//Q
			case 'Q':
				*ptro = (((int)3111**(ptr)-(int)5226**(ptr+1)+(int)2115**(ptr+2)+1332630)/10452);
				break;
				//u znormal
			case 'u':
				{
					double u = (886.0*(double)*(ptr)-587.0*(double)*(ptr+1)-299.0*(double)*(ptr+2));
					double bb = (114.0*(double)*(ptr)+587.0*(double)*(ptr+1)+299.0*(double)*(ptr+2)+1.0);
					*ptro = (uint8_t)((u/bb+1.0)*29.07);
				}
				break;
				//u znormal
			case 'v':
				{
					double v = (-114.0*(double)*(ptr)-587.0*(double)*(ptr+1)+701.0*(double)*(ptr+2));
					double bb = (114.0*(double)*(ptr)+587.0*(double)*(ptr+1)+299.0*(double)*(ptr+2)+1.0);
					*ptro = (uint8_t)((v/bb+1.0)*76.245);
				}
				break;
				//i znorm
			case 'i':
				{
					double i = (-321.3*(double)*(ptr)-274.4*(double)*(ptr+1)+595.7*(double)*(ptr+2));
					double bb = (114.0*(double)*(ptr)+587.0*(double)*(ptr+1)+299.0*(double)*(ptr+2)+1.0);
					*ptro = (uint8_t)((i/bb+2.8185)*53.006);
				}
				break;
				//q znorm
			case 'q':
				{
					double q = (311.1*(double)*(ptr)-522.6*(double)*(ptr+1)+211.5*(double)*(ptr+2));
					double bb = (114.0*(double)*(ptr)+587.0*(double)*(ptr+1)+299.0*(double)*(ptr+2)+1.0);
					*ptro = (uint8_t)((q/bb+0.8903)*70.457);
				}
				break;
				//H+180
			case 'h':
				{
					double u = ((886.0*(double)*(ptr)-587.0*(double)*(ptr+1)-299.0*(double)*(ptr+2))/886.0);
					double v = ((-114.0*(double)*(ptr)-587.0*(double)*(ptr+1)+701.0*(double)*(ptr+2))/701.0);
					if(u == 0 && v == 0) *ptro = 0;
					else
					{
						double h = atan2(v, u)-M_PI;
						if (h<0) h+=(2*M_PI);
						*ptro = (uint8_t)(h*40.58451049);
					}
				} break;
				//X (CIE XYZ)
				/*
				265 x {{0.4124, 0.3576,  0.1805},
				255 x {0.2126, 0.7152,  0.0722},
				234 x {0.0193, 0.1192,  0.9505}};
				*/
			case 'X':
				{
					double r = ((double)*(ptr+2))/255.0;
					if (r <= 0.04045) r = r / 12.92;
					else r = pow(((r + 0.055) / 1.055), 2.4);
					double g = ((double)*(ptr+1))/255.0;
					if (g <= 0.04045) g = g / 12.92;
					else g = pow(((g + 0.055) / 1.055), 2.4);
					double b = ((double)*(ptr))/255.0;
					if (b <= 0.04045) b = b / 12.92;
					else b = pow(((b + 0.055) / 1.055), 2.4);
					*ptro = (uint8_t)(105.162*r+91.188*g+46.0275*b);
				} break;
				//Y (CIE XYZ)
			case 'Y':
				{
					double r = ((double)*(ptr+2))/255.0;
					if (r <= 0.04045) r = r / 12.92;
					else r = pow(((r + 0.055) / 1.055), 2.4);
					double g = ((double)*(ptr+1))/255.0;
					if (g <= 0.04045) g = g / 12.92;
					else g = pow(((g + 0.055) / 1.055), 2.4);
					double b = ((double)*(ptr))/255.0;
					if (b <= 0.04045) b = b / 12.92;
					else b = pow(((b + 0.055) / 1.055), 2.4);
					*ptro = (uint8_t)(54.213*r+182.376*g+18.411*b);
				} break;
				//Z (CIE XYZ)
			case 'Z':
				{
					double r = ((double)*(ptr+2))/255.0;
					if (r <= 0.04045) r = r / 12.92;
					else r = pow(((r + 0.055) / 1.055), 2.4);
					double g = ((double)*(ptr+1))/255.0;
					if (g <= 0.04045) g = g / 12.92;
					else g = pow(((g + 0.055) / 1.055), 2.4);
					double b = ((double)*(ptr))/255.0;
					if (b <= 0.04045) b = b / 12.92;
					else b = pow(((b + 0.055) / 1.055), 2.4);
					*ptro = (uint8_t)(4.5162*r+27.8928*g+222.417*b);
				} break;

				//L (CIE Lab)
			case 'L':
				{
					double r = ((double)*(ptr+2))/255.0;
					if (r <= 0.04045) r = r / 12.92;
					else r = pow(((r + 0.055) / 1.055), 2.4);
					double g = ((double)*(ptr+1))/255.0;
					if (g <= 0.04045) g = g / 12.92;
					else g = pow(((g + 0.055) / 1.055), 2.4);
					double b = ((double)*(ptr))/255.0;
					if (b <= 0.04045) b = b / 12.92;
					else b = pow(((b + 0.055) / 1.055), 2.4);

					double yy = 0.2126*r+0.7152*g+0.0722*b;
					yy = pow(yy, 1.0 / 3.0);
					*ptro = (uint8_t)(255*yy);
				}break;

				//a (CIE Lab)
			case 'a':
				{
					double r = ((double)*(ptr+2))/255.0;
					if (r <= 0.04045) r = r / 12.92;
					else r = pow(((r + 0.055) / 1.055), 2.4);
					double g = ((double)*(ptr+1))/255.0;
					if (g <= 0.04045) g = g / 12.92;
					else g = pow(((g + 0.055) / 1.055), 2.4);
					double b = ((double)*(ptr))/255.0;
					if (b <= 0.04045) b = b / 12.92;
					else b = pow(((b + 0.055) / 1.055), 2.4);

					double xx = 0.43387*r+0.37622*g+0.18990*b;
					double yy = 0.2126*r+0.7152*g+0.0722*b;
					xx = pow(xx, 1.0 / 3.0);
					yy = pow(yy, 1.0 / 3.0);
					*ptro = (uint8_t)(376.128385*(xx-yy+0.33898));
				}break;
				//b (CIE Lab)
			case 'b':
				{
					double r = ((double)*(ptr+2))/255.0;
					if (r <= 0.04045) r = r / 12.92;
					else r = pow(((r + 0.055) / 1.055), 2.4);
					double g = ((double)*(ptr+1))/255.0;
					if (g <= 0.04045) g = g / 12.92;
					else g = pow(((g + 0.055) / 1.055), 2.4);
					double b = ((double)*(ptr))/255.0;
					if (b <= 0.04045) b = b / 12.92;
					else b = pow(((b + 0.055) / 1.055), 2.4);

					double yy = 0.2126*r+0.7152*g+0.0722*b;
					double zz = 0.01772*r+0.109458*g+0.872819*b;
					yy = pow(yy, 1.0 / 3.0);
					zz = pow(zz, 1.0 / 3.0);
					*ptro = (uint8_t)(159.235668*(yy-zz+0.8007));
				}break;
				//Brightness
			default:
				*ptro = (uint8_t)(((int)114**(ptr)+(int)587**(ptr+1)+(int)299**(ptr+2))/1000);
			}
			ptro++;
			ptr+=3;
		}
	}
}

// ----------------------------------------------------------------
// Functions for feature number retrieval
// Funkcje udostepniaja liczbe cech
// ----------------------------------------------------------------
int mzCountOfHistogramFeatures(void)
{
	return Lhist;
};
int mzCountOfArmFeatures(void)
{
	return Larm;
};
int mzCountOfGradientFeatures(void)
{
	return Lgrad;
};
int mzCountOfComFeatures(void)
{
	return Lcom;
};
int mzCountOfRlmFeatures(void)
{
	return Lrlm;
};

// ----------------------------------------------------------------
// Functions for feature names retrieval
// Funkcje udostepniaja nazwy cech
// ----------------------------------------------------------------
unsigned int mzNamesOfHistogramFeatures(unsigned int index, char* buffer, unsigned int buffersize)
{
	if(index>=Lhist) return 0;
	unsigned int sl = strlen(histnames[index]);
	if(sl >= buffersize) return sl;
	memcpy(buffer, histnames[index], sl+1);
	return sl;
};
unsigned int mzNamesOfArmFeatures(unsigned int index, char* buffer, unsigned int buffersize)
{
	if(index>=Larm) return 0;
	unsigned int sl = strlen(armnames[index]);
	if(sl >= buffersize) return sl;
	memcpy(buffer, armnames[index], sl+1);
	return sl;
};
unsigned int mzNamesOfGradientFeatures(unsigned int index, char* buffer, unsigned int buffersize)
{
	if(index>=Lgrad) return 0;
	unsigned int sl = strlen(gradnames[index]);
	if(sl >= buffersize) return sl;
	memcpy(buffer, gradnames[index], sl+1);
	return sl;
};
unsigned int mzNamesOfComFeatures(unsigned int index, char* buffer, unsigned int buffersize)
{
	if(index>=Lcom) return 0;
	unsigned int sl = strlen(comnames[index]);
	if(sl >= buffersize) return sl;
	memcpy(buffer, comnames[index], sl+1);
	return sl;
};
unsigned int mzNamesOfRlmFeatures(unsigned int index, char* buffer, unsigned int buffersize)
{
	if(index>=Lrlm) return 0;
	unsigned int sl = strlen(rlmnames[index]);
	if(sl >= buffersize) return sl;
	memcpy(buffer, rlmnames[index], sl+1);
	return sl;
};

// ----------------------------------------------------------------
// Functions for image intensity retrieval
// Funkcje jasnosci obrazu
// ----------------------------------------------------------------
inline uint8_t GetPixelMem(const unsigned int x, const unsigned int y, const uint8_t* input, const unsigned int linelength)
{
	return ((uint8_t*)input)[y*linelength+x];
};

/*------------------------------------------------------------------------*/
inline uint8_t GetPixelNorm(const unsigned int x, const unsigned int y, const unsigned int bpp, 
						 const int min, const int max, const uint8_t* input, const unsigned int linelength)
{
	if(min == 0 && max == 255)
	{
		if(bpp>8) return 0;
		else return GetPixelMem(x, y, input, linelength)>>(8-bpp);
	}
	else
	{
		int p = GetPixelMem(x, y, input, linelength);
		return mzMulDiv((p>max ?max-min :(p<min ?0 :p-min)), ((int)1<<(bpp))-1, max-min);
	}
};

// ----------------------------------------------------------------
// Function for ROI check
// Funkcja sprawdzania przynaleznosci do ROI
// ----------------------------------------------------------------
inline int IsWithinRoi(const unsigned int x, const unsigned int y, const int referx, const int refery, 
					   const uint8_t* roi, const unsigned int roiwidth, const unsigned int roiheight, const unsigned int linelengthroi)
{
// refer = center - shift
	unsigned int xx = (unsigned int)(x - referx);
	if(xx > roiwidth) return false;
	unsigned int yy = (unsigned int)(y - refery);
	if(yy > roiheight) return false;
	return (roi[xx+yy*linelengthroi]);
};
inline int IsWithinRoi(const unsigned int xx, const unsigned int yy, 
					   const uint8_t* roi, const unsigned int roiwidth, const unsigned int roiheight, const unsigned int linelengthroi)
{
	if(xx > roiwidth) return false;
	if(yy > roiheight) return false;
	return (roi[xx+yy*linelengthroi]);
};

// ----------------------------------------------------------------
// Function computes normalisation or standardization span 
// for the image histogram
// Funkcja oblicza zakresy jasnosci histogramu do celow
// jego normalizacji lub standaryzacji
// ----------------------------------------------------------------
unsigned int mzComputeSpan(int* min, int* max, char mode, 
				   unsigned int width, unsigned int height, const uint8_t* input, unsigned int linelength, 
				   const uint8_t* roi, unsigned int roiwidth, unsigned int roiheight, int referx, int refery, const unsigned int linelengthroi)
{
	unsigned long int Hist[0x100];
	unsigned int i;
	unsigned int area;

	for(i = 0; i < 0x100; i++) {Hist[i]=0;}
	area=0;

	for(unsigned int yy = 0; yy < roiheight; yy++)
	{
		unsigned int y = yy + refery;
		if(y < height) 
		{
			for(unsigned int xx = 0; xx < roiwidth; xx++)
			{
				unsigned int x = xx + referx;
				if(x < width) 
				{
					if(IsWithinRoi(xx, yy, roi, roiwidth, roiheight, linelengthroi))
					{
						uint8_t k = GetPixelMem(x, y, input, linelength);
						Hist[k]++;
						area++;
					}
				}
			}
		}
	}
	
	if(area <= 0)
	{
		*min = 0;
		*max = 255;
		return area;
	}		

	switch(mode)
	{
		case SPAN_STANDARDIZE:
		{
			double mean = 0.0;
			for(i = 0; i < 0x100; i++) mean +=(double)(i) * Hist[i];
			mean /= area;

			double stdev = 0.0;
			for(i = 0; i < 0x100; i++) stdev+=(double)(i-mean)*(i-mean)*Hist[i];
			stdev /= area;
			stdev = sqrt(stdev);

			*min = (int)(mean - (N_sigma*stdev));
			*max = (int)(mean + (N_sigma*stdev));
		}
		break;
		case SPAN_NORMALIZE:
		{
			for(i = 1; i < 0x100; i++) Hist[i] += Hist[i-1];
			double k01=(double)area/100;
			double k99=(double)area-k01;

			for(i = 0; i < 0x100; i++)
			{
				if(Hist[i]>=k01) break;
			}
			*min = i + 1;
			for(; i < 0x100; i++)
			{
				if(Hist[i]>=k99) break;
			}
			*max = i;
		}		
		break;

		case SPAN_UNCHANGE:
		default:
		{
			*min = 0;
			*max = 255;
		}		
		break;
	}
	return area;
};

// ----------------------------------------------------------------
// Function computes histogram features 
// Funkcja oblicza cechy histogramu
// ----------------------------------------------------------------
void mzComputeHistogramFeatures(double* features, const unsigned int area,
								unsigned int width, unsigned int height, const uint8_t* input, unsigned int linelength, 
								const uint8_t* roi, unsigned int roiwidth, unsigned int roiheight, int referx, int refery, const unsigned int linelengthroi)
{
	unsigned long int Hist[0x100];
//	unsigned long int Hiss[0x100];
	unsigned int i;
	if(area <= 0) return;
	for(i = 0; i < 0x100; i++) Hist[i]=0;

	for(unsigned int yy = 0; yy < roiheight; yy++)
	{
		unsigned int y = yy + refery;
		if(y < height) 
		{
			for(unsigned int xx = 0; xx < roiwidth; xx++)
			{
				unsigned int x = xx + referx;
				if(x < width) 
				{
					if(IsWithinRoi(xx, yy, roi, roiwidth, roiheight, linelengthroi))
					{
						uint8_t k = GetPixelMem(x, y, input, linelength);
						Hist[k]++;
					}
				}
			}
		}
	}

	double mean = 0.0;
	for(i = 0; i < 0x100; i++) mean += (double)(i) * Hist[i];
	mean /= area;
	features[0] = mean;

	double var = 0.0;
	for(i = 0; i < 0x100; i++) var += (double)(i-mean)*(i-mean)*Hist[i];
	var /= area;
	features[1] = var;
//	stdev = sqrt(stdev);
	if(var > 0)
	{
		double sk = 0.0;;
		for(i = 0; i < 0x100; i++) sk += (i-mean)*(i-mean)*(i-mean)*Hist[i];
		features[2] = (1.0/sqrt(var*var*var)) * sk / area;

		sk = 0.0;
		for(i = 0; i < 0x100; i++) sk += (i-mean)*(i-mean)*(i-mean)*(i-mean)*Hist[i];
		features[3] = (1.0/(var*var)) * sk / area - 3;
	}
	else
	{
		features[2] = 0.0;
		features[3] = 0.0;
	}
	
	double k01 =(double)area/100;
	double k10 =(double)area/10;
	double k50 =(double)area/2;
	double k90 =(double)area-k10;
	double k99 =(double)area-k01;

	unsigned long int maxim01 = 0;
	unsigned int domin01 = 0;
	for(i = 0; i < 0x100; i++)
	{
		if(Hist[i] > maxim01)
		{
			maxim01 = Hist[i];
			domin01 = i;
		}
	}
	for(i = 1; i < 0x100; i++) Hist[i] += Hist[i-1];

	unsigned long int maxim10 = 0;
	unsigned int domin10 = 0;
	for(i = 0; i < (0x100-0x10); i++)
	{
		if(Hist[i+0x10]-Hist[i] > maxim10)
		{
			maxim10 = Hist[i+0x10]-Hist[i];
			domin10 = i;
		}
	}

	for(i = 0; i < 0x100; i++)
	{
		if(Hist[i]>=k01) break;
	}
	features[4] = i;

	for(; i < 0x100; i++)
	{
		if(Hist[i]>=k10) break;
	}
	features[5] = i;
	for(; i < 0x100; i++)
	{
		if(Hist[i]>=k50) break;
	}
	features[6] = i;
	for(; i < 0x100; i++)
	{
		if(Hist[i]>=k90) break;
	}
	features[7] = i;
	for(; i < 0x100; i++)
	{
		if(Hist[i]>=k99) break;
	}
	features[8] = i;
	features[9] = (double)maxim01 / (double)area;
	features[10] = (double)(domin01);
	features[11] = (double)maxim10 / (double)area;
	features[12] = (double)(domin10);
}

// ----------------------------------------------------------------
// Function computes histogram features 
// Funkcja oblicza cechy histogramu
// ----------------------------------------------------------------
void mzComputeComFeatures(double* features, 
						  const unsigned int bpp, const int min, const int max, const bool symmetrize, const int distx, const int disty,
						  const unsigned int width, unsigned int height, const uint8_t* input, unsigned int linelength, 
						  const uint8_t* roi, const int roiwidth, const int roiheight, const int referx, const int refery, const unsigned int linelengthroi)
{
	unsigned int S[256][256];
	long int RR[512];
	int levels = (int)1<<bpp;
	if(levels >= 256) return;

	int i, j;
	for(i=0; i<levels; i++)	for(j=0; j<levels; j++)	S[i][j] = 0;
	int area = 0;

	//disty = -disty; //(0,0) -> bottom-left

	int xxm  = distx >= 0 ? 0 : (-distx);
	int xxd  = distx < 0 ? roiwidth : (roiwidth-distx);
	int yym  = disty >= 0 ? 0 : (-disty);
	int yyd  = disty < 0 ? roiheight : (roiheight-disty);

	for(int yy = yym; yy < yyd; yy++)
	{
		unsigned int y = yy + refery;
		if(y < height && ((unsigned int)(y+disty)) < height) 
		{
			for(int xx = xxm; xx < xxd; xx++)
			{
				unsigned int x = xx + referx;
				if(x < width && ((unsigned int)(x+distx)) < width)
				{
					if(IsWithinRoi(xx, yy, roi, roiwidth, roiheight, linelengthroi) &&
						IsWithinRoi(xx+distx, yy+disty, roi, roiwidth, roiheight, linelengthroi))

					{
						uint8_t k = GetPixelNorm(x, y, bpp, min, max, input, linelength);
						uint8_t l = GetPixelNorm(x+distx, y+disty, bpp, min, max, input, linelength);
						S[k][l]++; area++;
						if(symmetrize) 
						{
							S[l][k]++;
							area++;
						}
					}
				}
			}
		}
	}

	double c,cc,mx,my,vx,vy;
	int k,l;

	features[0]=(double)area;
// Moment zwykly 2 rzedu - cecha 0
	c=0;
	for(k=0; k<levels; k++)
	{
		for(l=0; l<levels; l++)
		{
			c+=((double)S[k][l]*(double)S[k][l]);
		}
	}
	features[1]=c/((double)area*area);

// Kontrast - cecha 1
	c=0;
	for(k=0; k<levels; k++)
	{
		for(l=0; l<levels; l++)
		{
			c+=(double)S[k][l]*((double)k-l)*((double)k-l);
		}
	}
	features[2]=c/(double)area;

// Korelacja - cecha 2
	mx=0; my=0; vx=0; vy=0; c=0;
	for(k=0; k<levels; k++)
	{
		for(l=0; l<levels; l++)
		{
			mx+=((double)k+1)*(double)S[k][l];
			my+=((double)l+1)*(double)S[k][l];
		}
	}
	mx=mx/area;  my=my/area;
	for(k=0; k<levels; k++)
	{
		for(l=0; l<levels; l++)
		{
			vx+=(mx-k-1)*(mx-k-1)*(double)S[k][l];
			vy+=(my-l-1)*(my-l-1)*(double)S[k][l];
		}
	}
	vx=vx/area;  vy=vy/area;
	
	for(k=0; k<levels; k++)
	{
		for(l=0; l<levels; l++)
		{
			c+=(double)S[k][l]*((double)k+1)*((double)l+1);
		}
	}
	features[3]=(c/(double)area-mx*my)/(vx*vy>Eps ?sqrt(vx*vy) :Eps);

	/* Wariancja rozkladu brzegowego px - cecha 3*/
	features[4]=vx;

	/* Odwrotny moment roznicowy - cecha 4*/
	c=0;
	for(k=0; k<levels; k++)
	{
		for(l=0; l<levels; l++)
		{
			c+=(double)S[k][l]/(1+(k-l)*(k-l));
		}
	}
	features[5]=c/area;

	/* Wartosc srednia rozkl. sumacyjnego - cecha 5*/
	mx=0;
	for(k=0; k<2*(levels)-1; k++)
	{
		RR[k]=0;
		for(l=(k-(levels)+1<0 ?0 :k-(levels)+1); ((l<(levels))&&(l<=k)); l++)
			RR[k]+=S[l][k-l];
		mx+=((double)(k+2)*RR[k])/(double)area;
	}
	features[6]=mx;

	/* Wariancja rozkl. sumacyjnego - cecha 6*/
	c=0;
	for(k=0; k<2*(levels)-1; k++)
	{
		cc=(double)(k+2-mx)*(k+2-mx);
		c+=RR[k]*cc;
	}
	features[7]=c/area;

	/* Entropia rozkl. sumacyjnego - cecha 7*/
	c=0;
	for(k=0; k<2*(levels)-1; k++)
	{
		cc=RR[k]/(double)area;
		c+=cc*(cc>0 ?log(cc)/Ln10 :(double)0);
	}
	features[8]=-c;

	/* Entropia - cecha 8*/
	c=0;
	for(k=0; k<(levels); k++)
	{
		for(l=0; l<(levels); l++)
		{
			cc=(double)S[k][l]/(double)area;
			c+=cc*(cc>0 ?log(cc)/Ln10 :(double)0);
		}
	}
	features[9]=-c;

// Obliczenie rozkladu roznicowego i jego mediany
	mx=0;
	for(k=0; k<(levels); k++)
	{
		RR[k]=0;
		for(l=0; l<(levels)-k; l++)
			RR[k]+=(k==0 ?S[l][l] :S[l][l+k]+S[l+k][l]);
		mx+=((double)RR[k]*(k+1))/(double)area;
	}

// Wariancja rozkl. roznicowego - cecha 9
	c=0;
	for(k=0; k<(levels); k++)
	{
		c+=(double)RR[k]*((double)k-mx+1)*((double)k-mx+1);
	}
	features[10]=c/((double)area);

	/* Entropia rozkl. roznicowego - cecha 10 */
	c=0;
	for(k=0; k<(levels); k++)
	{
		cc=(double)RR[k]/(double)area;
		c+=cc*(cc>0 ?log(cc)/Ln10 :0);
	}
	features[11]=-c;
}

// ----------------------------------------------------------------
// Function computes raw's length 
// Funkcja oblicza dlugosc ciagu pikseli
// ----------------------------------------------------------------
void mzComputeRlmFeaturesRaw(unsigned int x, unsigned int y, unsigned int xx, unsigned int yy, 
							const int dx, const int dy, const unsigned int bpp, const int min, const int max,
							const uint8_t* roi, const unsigned int roiwidth, const unsigned int roiheight, const unsigned int linelengthroi,  
							const uint8_t* input, const unsigned int width, const unsigned int height, const unsigned int linelength, 
							unsigned long int** RL)
{
	int count;
	int glevel;
	count = -1;
	glevel = -1;
	while(xx < roiwidth && yy < roiheight && x < width && y < height)
	{
		if(IsWithinRoi(xx, yy, roi, roiwidth, roiheight, linelengthroi))
		{
			if(glevel<0)
			{
				glevel = GetPixelNorm(x, y, bpp, min, max, input, linelength);
				count++;
			}
			else
			{
				int l = GetPixelNorm(x, y, bpp, min, max, input, linelength);
				if(glevel != l)
				{
					RL[glevel][count]++;
					count=0;
					glevel = l;
				}
				else
				{
					count++;
				}
			}
		}
		else
		{
			if(count>=0 && glevel>=0) RL[glevel][count]++;
			count = -1;
			glevel = -1;
		}
		x+=dx;
		y+=dy;
		xx+=dx;
		yy+=dy;
	}
	if(count>=0 && glevel>=0)
	{
		RL[glevel][count]++;
	}
};

// ----------------------------------------------------------------
// Function computes runlength features 
// Funkcja oblicza cechy ciagow pikseli
// ----------------------------------------------------------------
void mzComputeRlmFeatures(double* features, unsigned int area,
						  unsigned int bpp, int min, int max, char direction,
						  unsigned int width, unsigned int height, const uint8_t* input, unsigned int linelength, 
						  const uint8_t* roi, int roiwidth, int roiheight, int referx, int refery, const unsigned int linelengthroi)
{
	int levels = (int)1<<bpp;
	if(levels >= 256) return;
	unsigned long int **RL;

	int i, j, k;
	int rlsize = roiwidth > roiheight ?roiwidth :roiheight;
	RL = (unsigned long int**) malloc(sizeof(unsigned long int*)*levels);
	if(RL==NULL) return;
	for(k=0; k < levels; k++)
	{
		RL[k]=(unsigned long int*) malloc(sizeof(unsigned long int)*rlsize);
		if(RL[k]==NULL)
		{
			for(k--; k >= 0; k--) free(RL[k]);
			free(RL);
			return;
		}
		for(i = 0; i < rlsize; i++) RL[k][i] = 0;
	}

	int dx, dy, yy, xx;
	unsigned int x, y;
	switch (direction)
	{
	case 1:
	case DIRECTION_V:
		dx = 0;
		dy = 1;
		if(refery < 0)
		{
			y = 0;
			yy = y - refery;
		}
		else
		{
			yy = 0;
			y = yy + refery;
		}
		for(xx = 0; xx < roiwidth; xx++)
		{
			x = xx + referx;
			mzComputeRlmFeaturesRaw(x, y, xx, yy, dx, dy, bpp, min, max, roi, roiwidth, roiheight, linelengthroi, input, width, height, linelength, RL);
		}
		break;

	case 2:
	case DIRECTION_Z:
		dx = -1;
		dy = 1;
		yy = 0;
		y = yy + refery;
		for(xx = 0; xx < roiwidth; xx++)
		{
			x = xx + referx;
			mzComputeRlmFeaturesRaw(x, y, xx, yy, dx, dy, bpp, min, max, roi, roiwidth, roiheight, linelengthroi, input, width, height, linelength, RL);
		}
		xx = roiwidth - 1;
		x = xx + referx;
		for(yy = 1; yy < roiheight; yy++)
		{
			y = yy + refery;
			mzComputeRlmFeaturesRaw(x, y, xx, yy, dx, dy, bpp, min, max, roi, roiwidth, roiheight, linelengthroi, input, width, height, linelength, RL);
		}
		break;

	case 3:
	case DIRECTION_N:
		dx = -1;
		dy = 1;
		yy = roiheight - 1;
		y = yy + refery;
		for(xx = 0; xx < roiwidth; xx++)
		{
			x = xx + referx;
			mzComputeRlmFeaturesRaw(x, y, xx, yy, dx, dy, bpp, min, max, roi, roiwidth, roiheight, linelengthroi, input, width, height, linelength, RL);
		}
		xx = roiwidth - 1;
		x = xx + referx;
		for(yy = 0; yy < roiheight-1; yy++)
		{
			y = yy + refery;
			mzComputeRlmFeaturesRaw(x, y, xx, yy, dx, dy, bpp, min, max, roi, roiwidth, roiheight, linelengthroi, input, width, height, linelength, RL);
		}
		break;

//	case 0:
//	case DIRECTION_H:
	default:
		dx = -1;
		dy = 0;
		xx = roiwidth - 1;
		x = xx + referx;
		for(yy = 1; yy < roiheight; yy++)
		{
			y = yy + refery;
			mzComputeRlmFeaturesRaw(x, y, xx, yy, dx, dy, bpp, min, max, roi, roiwidth, roiheight, linelengthroi, input, width, height, linelength, RL);
		}
		break;
	}

	double ddw, dw, c;
	c=0;
	for (i = 0; i < levels; i++)
	{
		for (j = 0; j < rlsize; j++)
		{
			c+=RL[i][j];
		}
	}
	if(!c) return;

	/*RLDist*/
	ddw=0;
	for (j = 0; j < rlsize; j++)
	{
		dw = 0;
		for (i = 0; i < levels; i++)
			dw += RL[i][j];
		ddw += (double) dw * dw;
	}
	features[0]=(double)ddw/c;

	/*GDist*/
	ddw=0;
	for (i = 0; i < levels; i++)
	{
		dw = 0;
		for (j = 0; j < rlsize; j++)
			dw += RL[i][j];
		ddw += (double) dw * dw;
	}
	features[1]=(double)ddw/c;

	/*LRE*/
	ddw=0;
	for (i = 0; i < levels; i++)
		for (j = 0; j < rlsize; j++)
			ddw += (double) (j+1)*(j+1)*RL[i][j];
	features[2]=(double)ddw/c;

	/*Short Run Emphasis*/
	ddw=0;
	for (i = 0; i < levels; i++)
		for (j = 0; j < rlsize; j++)
			ddw += (double) RL[i][j]/((double)(j+1)*(j+1));
	features[3]=(double)ddw/c;

	/*Fraction of Image in Runs*/
	ddw=0;
	for (i = 0; i < levels; i++)
		for (j = 0; j < rlsize; j++)
			ddw += (double)(j+1)*RL[i][j];
	features[4]=(double)c/ddw;

	for(k=0; k < levels; k++) free(RL[k]);
    free(RL);
};

// ----------------------------------------------------------------
// Function computes gradient features 
// Funkcja oblicza cechy gradientowe
// ----------------------------------------------------------------
void mzComputeGradientFeatures(double* features, const unsigned int area,
						  const unsigned int bpp, int min, int max,
						  const int width, const int height, const uint8_t* input, const unsigned int linelength, 
						  const uint8_t* roi, const int roiwidth, const int roiheight, const int referx, const int refery, const unsigned int linelengthroi)
{
	double* GR;
	GR = (double*) malloc(sizeof(double)*area);
	int aream = 0;

	for(int yy = 1; yy < roiheight - 1; yy++)
	{
		int y = yy + refery;
		if(y < height - 1 && y > 0) 
		{
			for(int xx = 0; xx < roiwidth; xx++)
			{
				int x = xx + referx;
				if(x < width - 1 && x > 0) 
				{
					if(IsWithinRoi(xx, yy, roi, roiwidth, roiheight, linelengthroi) &&
						IsWithinRoi(xx+1, yy, roi, roiwidth, roiheight, linelengthroi) &&
						IsWithinRoi(xx-1, yy, roi, roiwidth, roiheight, linelengthroi) &&
						IsWithinRoi(xx, yy+1, roi, roiwidth, roiheight, linelengthroi) &&
						IsWithinRoi(xx, yy-1, roi, roiwidth, roiheight, linelengthroi))
					{
						double v, h;
						v = (int)GetPixelNorm(x, y+1, bpp, min, max, input, linelength)
						  - (int)GetPixelNorm(x, y-1, bpp, min, max, input, linelength);
						h = (int)GetPixelNorm(x+1, y, bpp, min, max, input, linelength)
						  - (int)GetPixelNorm(x-1, y, bpp, min, max, input, linelength);
						GR[aream] = sqrt(v*v + h*h);
						aream ++;
					}
				}
			}
		}
	}

	double c,cc, m, ro, pro, ro3, ro4;
	long int k00, k01, k10, k50, k90, k;

	features[0] = aream;
	if(aream <= 0) return;

	/* Wart. srednia - cecha 0*/
	m=0;
	for(k=0; k<aream; k++)
		m+=(double)GR[k];
	features[1]=m=m/aream;

	/* Warincja - cecha 1*/
	ro=0;
	for(k=0; k<aream; k++)
	{
		c=GR[k]-m;
		ro+=(double)c*c;
	}
	features[2]=ro=ro/aream;
	pro=sqrt(ro);

	if(ro)
	{
		/* Zeskosowanie - cecha 2*/
		c=0;
		for(k=0; k<aream; k++)
		{
			cc=GR[k]-m;
			c+=cc*cc*cc;
		}
		ro3=(double)1/(pro*pro*pro);
		features[3]=ro3*c/aream;

		/* Kurtoza - cecha 3*/
		c=0;
		for(k=0; k<aream; k++)
		{
			cc=GR[k]-m;
			c+=cc*cc*cc*cc;
		}
		ro4=(double)1/(ro*ro);
		features[4]=ro4*c/aream-3;
	}

	k01=0; k10=0; k50=0; k90=0;
	k00=0;
	for(k=0; k<aream; k++)
	{
		cc=GR[k];
		if(cc>0.0) k00++;
		if(cc>0.9) k90++;
		if(cc>0.01) k01++;
		if(cc>0.1) k10++;
		if(cc>0.5) k50++;
	}

	features[5]=(double)k00/aream;
	features[6]=(double)k01/aream;
	features[7]=(double)k10/aream;
	features[8]=(double)k50/aream;
	features[9]=(double)k90/aream;
	
	free(GR);
};


double M1[4][4];
double M2[4][4];
double M3[4][4];
double V1[4];
double V2[4];
double V3[4];

/*------------------------------------------------------------------------*/
double wyzn4(double W[4][4])
{
  return
      W[0][0]*
      (
         W[1][1]*(W[2][2]*W[3][3]-W[2][3]*W[3][2])
        -W[1][2]*(W[2][1]*W[3][3]-W[2][3]*W[3][1])
        +W[1][3]*(W[2][1]*W[3][2]-W[2][2]*W[3][1])
      )
     -W[0][1]*
      (
         W[1][0]*(W[2][2]*W[3][3]-W[2][3]*W[3][2])
        -W[1][2]*(W[2][0]*W[3][3]-W[2][3]*W[3][0])
        +W[1][3]*(W[2][0]*W[3][2]-W[2][2]*W[3][0])
      )
     +W[0][2]*
      (
         W[1][0]*(W[2][1]*W[3][3]-W[2][3]*W[3][1])
        -W[1][1]*(W[2][0]*W[3][3]-W[2][3]*W[3][0])
        +W[1][3]*(W[2][0]*W[3][1]-W[2][1]*W[3][0])
      )
     -W[0][3]*
      (
         W[1][0]*(W[2][1]*W[3][2]-W[2][2]*W[3][1])
        -W[1][1]*(W[2][0]*W[3][2]-W[2][2]*W[3][0])
        +W[1][2]*(W[2][0]*W[3][1]-W[2][1]*W[3][0])
      );
}

/*------------------------------------------------------------------------*/
double pod3wyzn4(double W[4][4], int w, int k)
{
  int w0, w1, w2;
  int k0, k1, k2;

  w0=0; if(w0==w) w0++;
  w1=w0+1; if(w1==w) w1++;
  w2=w1+1; if(w2==w) w2++;

  k0=0; if(k0==k) k0++;
  k1=k0+1; if(k1==k) k1++;
  k2=k1+1; if(k2==k) k2++;

  return
         W[w0][k0]*(W[w1][k1]*W[w2][k2]-W[w1][k2]*W[w2][k1])
        -W[w0][k1]*(W[w1][k0]*W[w2][k2]-W[w1][k2]*W[w2][k0])
        +W[w0][k2]*(W[w1][k0]*W[w2][k1]-W[w1][k1]*W[w2][k0]);
}

/*------------------------------------------------------------------------*/
void ma_wekwek4(double W[4][4], double W1[4], double W2[4])
{
  int k, l;
  for(k=0; k<4; k++)
    for(l=0; l<4; l++)
      W[k][l]=W1[k]*W2[l];
}

/*------------------------------------------------------------------------*/
double li_wekwek4(double W1[4], double W2[4])
{
  return W1[0]*W2[0]+W1[1]*W2[1]+W1[2]*W2[2]+W1[3]*W2[3];
}

/*------------------------------------------------------------------------*/
void ma_ma4(double W[4][4], double W1[4][4], double W2[4][4])
{
  int k, l;

  for(k=0; k<4; k++)
    for(l=0; l<4; l++)
      W[k][l]=W1[k][0]*W2[0][l]+W1[k][1]*W2[1][l]
             +W1[k][2]*W2[2][l]+W1[k][3]*W2[3][l];
}

/*------------------------------------------------------------------------*/
void wek_mawek4(double W[4], double W1[4][4], double W2[4])
{
  int k;

  for(k=0; k<4; k++)
    W[k]=W1[k][0]*W2[0]+W1[k][1]*W2[1]
             +W1[k][2]*W2[2]+W1[k][3]*W2[3];
}

/*------------------------------------------------------------------------*/
void sum_ma4(double W[4][4], double W1[4][4])
{
  int k, l;
  for(k=0; k<4; k++)
    for(l=0; l<4; l++)
      W[k][l]+=W1[k][l];
}

/*------------------------------------------------------------------------*/
void sum_wek4(double W[4], double W1[4])
{
  int l;
  for(l=0; l<4; l++)
    W[l]+=W1[l];
}

/*------------------------------------------------------------------------*/
int invert4(double W[4][4], double W1[4][4])
{
  int k, l;
  double det;
  det=wyzn4(W1);
  if(det==0) return 1;

  for(k=0; k<4; k++)
    for(l=0; l<4; l++)
    {
      if((k+l)%2) W[l][k]=-pod3wyzn4(W1, k, l)/det;
      else W[l][k]=pod3wyzn4(W1, k, l)/det;
    }
  return 0;
}

// ----------------------------------------------------------------
// Function computes gradient features 
// Funkcja oblicza cechy gradientowe
// ----------------------------------------------------------------
void mzComputeArmFeatures(double* features, 
						  const unsigned int width, const unsigned int height, const uint8_t* input, const unsigned int linelength, 
						  const uint8_t* roi, const unsigned int roiwidth, const unsigned int roiheight, const int referx, const int refery, const unsigned int linelengthroi)
{
	int i,j;
	long int area;
	double sigma;
	double ds, si;
	double vrnc, mean;

	area = 0;
	sigma = 0.0;
	mean = 0.0;
	vrnc = 0.0;

	for(unsigned int yy = 0; yy < roiheight; yy++)
	{
		unsigned int y = yy + refery;
		if(y < height) 
		{
			for(unsigned int xx = 0; xx < roiwidth; xx++)
			{
				unsigned int x = xx + referx;
				if(x < width) 
				{
					if(IsWithinRoi(xx, yy, roi, roiwidth, roiheight, linelengthroi))
					{
						int le = GetPixelMem(x, y, input, linelength);
						mean += le;
						vrnc += (le * le);
						area++;
					}
				}
			}
		}
	}

	features[0] = area;
    if (area <= 0) return;
	mean /= area;
	vrnc /= area;
	vrnc -= (mean * mean);

    if (vrnc <= 0) return;
	ds=sqrt(vrnc);
    for(i=0; i<4; i++)
	{
		for(j=0; j<4; j++) M1[j][i]=0;
		V1[i]=0;
	}
    area = 0;

	
	for(unsigned int yy = 0; yy < roiheight; yy++)
	{
		unsigned int y = yy + refery;
		if(y < height && y - 1 < height) 
		{
			for(unsigned int xx = 0; xx < roiwidth; xx++)
			{
				unsigned int x = xx + referx;
				if(x - 1 < width && x + 1 < width) 
				{
					if(IsWithinRoi(xx, yy, roi, roiwidth, roiheight, linelengthroi) &&
						IsWithinRoi(xx-1, yy, roi, roiwidth, roiheight, linelengthroi) &&
						IsWithinRoi(xx, yy-1, roi, roiwidth, roiheight, linelengthroi) &&
						IsWithinRoi(xx-1, yy-1, roi, roiwidth, roiheight, linelengthroi) &&
						IsWithinRoi(xx+1, yy-1, roi, roiwidth, roiheight, linelengthroi))
					{
						double fs;
						fs = ((double)GetPixelMem(x, y, input, linelength) - mean)/ds;

						V2[0] = ((double)GetPixelMem(x-1, y, input, linelength) - mean)/ds;
						V2[1] = ((double)GetPixelMem(x-1, y-1, input, linelength) - mean)/ds;
						V2[2] = ((double)GetPixelMem(x, y-1, input, linelength) - mean)/ds;
						V2[3] = ((double)GetPixelMem(x+1, y-1, input, linelength) - mean)/ds;
						
						V3[0]=V2[0]*fs;
						V3[1]=V2[1]*fs;
						V3[2]=V2[2]*fs;
						V3[3]=V2[3]*fs;

						sum_wek4(V1, V3);
						ma_wekwek4(M2, V2, V2);
						sum_ma4(M1, M2);
						area++;
					}
				}
			}
		}
	}
	
	if(invert4(M2, M1)) return;

	wek_mawek4(V2, M2, V1);
    for(i = 0; i < 4; i++) features[i+1] = V2[i];


	for(unsigned int yy = 0; yy < roiheight; yy++)
	{
		unsigned int y = yy + refery;
		if(y < height && y - 1 < height) 
		{
			for(unsigned int xx = 0; xx < roiwidth; xx++)
			{
				unsigned int x = xx + referx;
				if(x - 1 < width && x + 1 < width) 
				{
					if(IsWithinRoi(xx, yy, roi, roiwidth, roiheight, linelengthroi) &&
						IsWithinRoi(xx-1, yy, roi, roiwidth, roiheight, linelengthroi) &&
						IsWithinRoi(xx, yy-1, roi, roiwidth, roiheight, linelengthroi) &&
						IsWithinRoi(xx-1, yy-1, roi, roiwidth, roiheight, linelengthroi) &&
						IsWithinRoi(xx+1, yy-1, roi, roiwidth, roiheight, linelengthroi))
					{
						double fs;
						fs = ((double)GetPixelMem(x, y, input, linelength) - mean)/ds;

						V1[0] = ((double)GetPixelMem(x-1, y, input, linelength) - mean)/ds;
						V1[1] = ((double)GetPixelMem(x-1, y-1, input, linelength) - mean)/ds;
						V1[2] = ((double)GetPixelMem(x, y-1, input, linelength) - mean)/ds;
						V1[3] = ((double)GetPixelMem(x+1, y-1, input, linelength) - mean)/ds;
						
						si = fs-li_wekwek4(V1, V2);
						sigma += (si*si);					
					}
				}
			}
		}
	}
    features[5] = sqrt(sigma/(double)area);
};
