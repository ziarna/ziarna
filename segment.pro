#-------------------------------------------------
#
# Project created by QtCreator 2013-08-31T08:44:48
#
#-------------------------------------------------

TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    segment.cpp

unix|win32: LIBS += -lopencv_core
unix|win32: LIBS += -lopencv_imgproc
unix|win32: LIBS += -lopencv_highgui
unix|win32: LIBS += -lopencv_legacy

