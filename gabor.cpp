/*
* MaZdaCl - computes image texture, shape and color descriptors
*
* Copyright 1998-2013 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include <stdio.h>
#include <string.h>
#define _USE_MATH_DEFINES 
#include <math.h>
#include <stdint.h>


int kernel_size =21;
const char gdir[] = "VvZzHhNn";
const int Lgab = 40;

double mzComputeGaborFeature(unsigned int index, unsigned int width, unsigned int height, const uint8_t* input, unsigned int linelength, int posx, int posy)
{
	double var = 10.0;
	double psi1 = M_PI*45.0/180.0;
	double psi2 = M_PI*315.0/180.0;
	int f = 0;
	if(index>=Lgab) return 0;
	int ph = index/5;
	int pu = index%5;

	double phase = (double)ph*M_PI/8.0;
	double w = (double)pu/2.0;
	double magav;
	double d1 = 0.0;
	double d2 = 0.0;
	for(int x = -kernel_size/2;x<=kernel_size/2; x++) 
	{
		int xx = x+posx;
		if(xx >= (int)width) xx = width-1;
		if(xx < 0) xx = 0;
		for(int y = -kernel_size/2;y<=kernel_size/2; y++) 
		{
			double kernel1 = exp( -((x*x)+(y*y))/(2*var))*cos( w*x*cos(phase)+w*y*sin(phase)+psi1);
			double kernel2 = exp( -((x*x)+(y*y))/(2*var))*cos( w*x*cos(phase)+w*y*sin(phase)+psi2);
			int yy = y+posy;
			if(yy >= (int)height) yy = height-1;
			if(yy < 0) yy = 0;
			double image = input[yy*linelength+xx];

			d1 += (image * kernel1);
			d2 += (image * kernel2);
		}
	}
	magav = sqrt(d1*d1 + d2*d2);
	return magav;
}


double mzComputeFFTGaborFeature(int index, IplImage * img, int posx, int posy)
{
	double var = 10.0;
	double psi1 = M_PI*45.0/180.0;
	double psi2 = M_PI*315.0/180.0;
	int f = 0;
	static int ph = 0;
	static int pu = 0;
	static IplImage* magn = NULL;

	if(magn == NULL)
	{
		magn = cvCreateImage(cvSize(img->width,img->height),IPL_DEPTH_32F,1);
		cvZero(magn);
	}

	if(index>=Lgab) return 0;
	if(index>=0)
	{
		ph = index/5;
		pu = index%5;
		CvMat * kernel1 = cvCreateMat(kernel_size,kernel_size,CV_32FC1);
		CvMat * kernel2 = cvCreateMat(kernel_size,kernel_size,CV_32FC1);
		IplImage* src = cvCreateImage(cvSize(img->width,img->height),IPL_DEPTH_32F,1);
		IplImage* dest1 = cvCloneImage(src);
		IplImage* dest2 = cvCloneImage(src);
		IplImage* dest1_mag = cvCloneImage(src);
		IplImage* dest2_mag = cvCloneImage(src);

		dest1 = cvCloneImage(src);
		dest2 = cvCloneImage(src);
		dest1_mag = cvCloneImage(src);
		dest2_mag = cvCloneImage(src);
		double phase = (double)ph*M_PI/8.0;
		double w = (double)pu/2.0;
		for(int x = -kernel_size/2;x<=kernel_size/2; x++) 
		{
			for(int y = -kernel_size/2;y<=kernel_size/2; y++) 
			{
				double k1 = exp( -((x*x)+(y*y))/(2*var))*cos( w*x*cos(phase)+w*y*sin(phase)+psi1);
				double k2 = exp( -((x*x)+(y*y))/(2*var))*cos( w*x*cos(phase)+w*y*sin(phase)+psi2);
				cvSet2D(kernel1,y+kernel_size/2,x+kernel_size/2,cvScalar(k1));
				cvSet2D(kernel2,y+kernel_size/2,x+kernel_size/2,cvScalar(k2));
			}
		}
		cvFilter2D(src, dest1, kernel1, cvPoint(-1,-1));
		cvFilter2D(src, dest2, kernel2, cvPoint(-1,-1));
	    cvPow(dest1,dest1_mag,2);
	    cvPow(dest2,dest2_mag,2);
		cvAdd(dest1_mag, dest2_mag, dest1);
	    cvPow(dest1, magn, 0.5);

		cvReleaseImage(&dest2_mag);
		cvReleaseImage(&dest1_mag);
		cvReleaseImage(&dest2);
		cvReleaseImage(&dest1);
		cvReleaseImage(&src);
		cvReleaseMat(&kernel1);
		cvReleaseMat(&kernel2);
	}
	
	double ret = 0.0;
	if(magn != NULL)
	{
		CvScalar cs = cvGet2D(magn, posy, posx);
		ret = cs.val[0];
	}
	return ret;
}


/*
void mzComputeGaborFeatures(double* features, unsigned int width, unsigned int height, const uint8_t* input, unsigned int linelength, int posx, int posy)
{
	double var = 10.0;
	double psi1 = M_PI*45.0/180.0;
	double psi2 = M_PI*315.0/180.0;

	int f = 0;
	for(int ph = 0; ph < 8; ph++)
	{
		double phase = (double)ph*M_PI/8.0;
		for(int pu = 1; pu <= 5; pu++)
		{
			double w = (double)pu/2.0;
			double magav;
			double d1 = 0.0;
			double d2 = 0.0;
			for(int x = -kernel_size/2;x<=kernel_size/2; x++) 
			{
				int xx = x+posx;
				if(xx >= (int)width) xx = width-1;
				if(xx < 0) xx = 0;
				for(int y = -kernel_size/2;y<=kernel_size/2; y++) 
				{
					double kernel1 = exp( -((x*x)+(y*y))/(2*var))*cos( w*x*cos(phase)+w*y*sin(phase)+psi1);
					double kernel2 = exp( -((x*x)+(y*y))/(2*var))*cos( w*x*cos(phase)+w*y*sin(phase)+psi2);
					int yy = y+posy;
					if(yy >= (int)height) yy = height-1;
					if(yy < 0) yy = 0;
					double image = input[yy*linelength+xx];

					d1 += (image * kernel1);
					d2 += (image * kernel2);
				}
			}
			magav = sqrt(d1*d1 + d2*d2);
			features[f] = magav;
			f++;
		}
	}
}   
*/
// ----------------------------------------------------------------
// Functions for feature number retrieval
// Funkcje udostepniaja liczbe cech
// ----------------------------------------------------------------
int mzCountOfGaborFeatures(void)
{
	return Lgab;
};
// ----------------------------------------------------------------
// Functions for feature names retrieval
// Funkcje udostepniaja nazwy cech
// ----------------------------------------------------------------
unsigned int mzNamesOfGaborFeatures(unsigned int index, char* buffer, unsigned int buffersize)
{
	if(index>=Lgab) return 0;
	if(buffersize < 8) return 8;
	int d = index/5;
	int i = index%5;
	sprintf(buffer, "Gab%c%i", gdir[d], (i+1));
	unsigned int sl = strlen(buffer);
	return sl;
}



/*
void mzComputeGaborFeatures(double* features, int area, IplImage* src, IplImage* roi, int referx, int refery)
{
	int x,y;
	double kernel_val;
	double var = 10.0;
	double psi1 = CV_PI*45.0/180.0;
	double psi2 = CV_PI*315.0/180.0;
	CvMat * kernel=0;
	IplImage* dest1 = 0;
	IplImage* dest2 = 0;
	
	dest1 = cvCloneImage(src);
	dest2 = cvCloneImage(src);
	kernel = cvCreateMat(kernel_size,kernel_size,CV_32FC1);

	int f = 0;
	for(int ph = 0; ph < 4; ph++)
	{
		double phase = (double)ph*CV_PI/4.0;
		for(int pu = 2; pu <= 6; pu++)
		{
			double w = (double)pu/2.0;
			cvZero(kernel);
			for (x = -kernel_size/2;x<=kernel_size/2; x++) {
				for (y = -kernel_size/2;y<=kernel_size/2; y++) {
					kernel_val = exp( -((x*x)+(y*y))/(2*var))*cos( w*x*cos(phase)+w*y*sin(phase)+psi1);
					cvSet2D(kernel,y+kernel_size/2,x+kernel_size/2,cvScalar(kernel_val));
				}
			}
			cvFilter2D(src, dest1, kernel, cvPoint(-1,-1));
			cvZero(kernel);
			for (x = -kernel_size/2;x<=kernel_size/2; x++) {
				for (y = -kernel_size/2;y<=kernel_size/2; y++) {
					kernel_val = exp( -((x*x)+(y*y))/(2*var))*cos( w*x*cos(phase)+w*y*sin(phase)+psi2);
					cvSet2D(kernel,y+kernel_size/2,x+kernel_size/2,cvScalar(kernel_val));
				}
			}
			cvFilter2D(src, dest2, kernel, cvPoint(-1,-1));

			double magav = 0.0;
			for (int yy = 0; yy < roi->height; yy++) 
			{
				unsigned int y = yy + refery;
				for (int xx = 0; xx < roi->width; xx++) 
				{
					unsigned int x = xx + referx;
					CvScalar gg = cvGet2D(roi, yy, xx);
					if(gg.val[0] > 0)
					{
						CvScalar i1 = cvGet2D(dest1, y, x);
						CvScalar i2 = cvGet2D(dest2, y, x);
						double d1 = i1.val[0];
						double d2 = i2.val[0];
						magav += sqrt(d1*d1 + d2*d2);
					}
				}
			}
			features[f] = magav /(double)area;
			f++;
		}
	}

	cvReleaseMat(&kernel);
	cvReleaseImage(&dest1);
	cvReleaseImage(&dest2);
}   
*/