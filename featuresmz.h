/*****************************************************************/
/*                                                               */
/* MODULE: Features.h                                            */
/*                                                               */
/* FUNCTIONALITY:                                                */
/*   Header for a textural feature computation module            */
/*                                                               */
/* AUTHOR:                                                       */
/*   Piotr M. Szczypinski                                        */
/*   Email: pms@p.lodz.pl                                        */
/*                                                               */
/* Copyright (C) 1998-2011 by Piotr M. Szczypinski               */
/*                                                               */
/*****************************************************************/
#ifndef _TEXTUREFEATURES_H
#define _TEXTUREFEATURES_H

#define SPAN_UNCHANGE 'D'
#define SPAN_STANDARDIZE 'S'
#define SPAN_NORMALIZE 'N'

#define DIRECTION_H 'H'
#define DIRECTION_V 'V'
#define DIRECTION_Z 'Z'
#define DIRECTION_N 'N'
const char mzDirections[9] = "HVZNhvzn";
const int mzVectors[8][2] = {{0,1}, {1,0}, {1,1}, {1,-1}, {0,-1}, {-1,0}, {-1,-1}, {-1,1}};
//const char mzChannels[] = "oRGBUVHSIQuviqhXYZLab"; //"oRGBUVHSIQuviqhXYZLab"

#include <stdint.h>

void mzImageConvert(char colortransform, int linelengthi, int linelengtho, int width, int height, void* input, uint8_t* output);

int mzCountOfHistogramFeatures(void);
int mzCountOfArmFeatures(void);
int mzCountOfGradientFeatures(void);
int mzCountOfComFeatures(void);
int mzCountOfRlmFeatures(void);

unsigned int mzNamesOfHistogramFeatures(unsigned int index, char* buffer, unsigned int buffersize);
unsigned int mzNamesOfArmFeatures(unsigned int index, char* buffer, unsigned int buffersize);
unsigned int mzNamesOfGradientFeatures(unsigned int index, char* buffer, unsigned int buffersize);
unsigned int mzNamesOfComFeatures(unsigned int index, char* buffer, unsigned int buffersize);
unsigned int mzNamesOfRlmFeatures(unsigned int index, char* buffer, unsigned int buffersize);

unsigned int mzComputeSpan(int* min, int* max, char mode, 
				   unsigned int width, unsigned int height, const uint8_t* input, unsigned int linelength, 
				   const uint8_t* roi, unsigned int roiwidth, unsigned int roiheight, int referx, int refery, const unsigned int linelengthroi);

void mzComputeHistogramFeatures(double* features, const unsigned int area,
								unsigned int width, unsigned int height, const uint8_t* input, unsigned int linelength, 
								const uint8_t* roi, unsigned int roiwidth, unsigned int roiheight, int referx, int refery, const unsigned int linelengthroi);

void mzComputeComFeatures(double* features, 
						  const unsigned int bpp, const int min, const int max, const bool symmetrize, const int distx, const int disty,
						  const unsigned int width, unsigned int height, const uint8_t* input, unsigned int linelength, 
						  const uint8_t* roi, const int roiwidth, const int roiheight, const int referx, const int refery, const unsigned int linelengthroi);

void mzComputeRlmFeatures(double* features, unsigned int area,
						  unsigned int bpp, int min, int max, char direction,
						  unsigned int width, unsigned int height, const uint8_t* input, unsigned int linelength, 
						  const uint8_t* roi, int roiwidth, int roiheight, int referx, int refery, const unsigned int linelengthroi);

void mzComputeGradientFeatures(double* features, const unsigned int area,
						  const unsigned int bpp, int min, int max,
						  const int width, const int height, const uint8_t* input, const unsigned int linelength, 
						  const uint8_t* roi, const int roiwidth, const int roiheight, const int referx, const int refery, const unsigned int linelengthroi);

void mzComputeArmFeatures(double* features, 
						  const unsigned int width, const unsigned int height, const uint8_t* input, const unsigned int linelength, 
						  const uint8_t* roi, const unsigned int roiwidth, const unsigned int roiheight, const int referx, const int refery, const unsigned int linelengthroi);


#endif //_TEXTUREFEATURES_H
