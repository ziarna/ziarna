TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += mazdacl.cpp\
           gabor.cpp\
           geompars.cpp\
    featuresmz.cpp

HEADERS += gabor.h\
           geompars.h\
    featuresmz.h
           
unix|win32: LIBS += -lopencv_core
unix|win32: LIBS += -lopencv_imgproc
unix|win32: LIBS += -lopencv_highgui
unix|win32: LIBS += -lopencv_legacy
