/*
* MaZdaCl - computes image texture, shape and color descriptors
*
* Copyright 1998-2013 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#define _USE_MATH_DEFINES 
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

const double mznan = HUGE_VAL;

struct struct_BASIC
{
	bool valid;
	double X;
	double Y;
	double F;
	double Spol;
}BASIC;	

struct struct_BOUNDING
{
	bool valid;
	CvMemStorage* storage;
	CvSeq* contours;
	int contours_num;
	double area;
	double perim;
	CvBox2D box_elli;
	CvBox2D box_rect;
	CvPoint2D32f center;
	float radius;
}BOUNDING;	

struct struct_MOMENTS
{
	bool valid;
	CvMoments moments;
}MOMENTS;

struct struct_CONVEX
{
	bool valid;
	double area;
	double perim;
	double maxcavity;
	int num_cavities;
	double sumcavity;
}CONVEX;

void GeomFeaturesInvalidate(void)
{
	BASIC.valid = false;
	BOUNDING.valid = false;
	MOMENTS.valid = false;
	CONVEX.valid = false;
}

void Compute_MOMENTS(IplImage* roi)
{
	if(MOMENTS.valid) return;
	cvMoments(roi, & MOMENTS.moments, 1);
	MOMENTS.valid = true;
}

void Compute_BOUNDING(IplImage* roi)
{
	if(BOUNDING.valid) return;

	BOUNDING.storage = cvCreateMemStorage(0);
	BOUNDING.contours = 0;
	BOUNDING.contours_num = cvFindContours(roi, BOUNDING.storage, &BOUNDING.contours, sizeof(CvContour), CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cvPoint(0,0));
	if(BOUNDING.contours_num > 0)
	{
		BOUNDING.perim = cvArcLength(BOUNDING.contours);
		BOUNDING.area = cvContourArea(BOUNDING.contours);
		BOUNDING.box_elli = cvFitEllipse2(BOUNDING.contours);
		BOUNDING.box_rect = cvMinAreaRect2(BOUNDING.contours); 
		cvMinEnclosingCircle(BOUNDING.contours, &BOUNDING.center, &BOUNDING.radius);
	}
	else
	{
		BOUNDING.perim = 0.0;
		BOUNDING.area = 0.0;
		BOUNDING.radius = 0.0;
		BOUNDING.box_rect.size.height = 0;
		BOUNDING.box_rect.size.width = 0;
		BOUNDING.box_rect.center.x = 0;
		BOUNDING.box_rect.center.y = 0;
		BOUNDING.box_rect.angle = 0;

		BOUNDING.box_elli.size.height = 0;
		BOUNDING.box_elli.size.width = 0;
		BOUNDING.box_elli.center.x = 0;
		BOUNDING.box_elli.center.y = 0;
		BOUNDING.box_elli.angle = 0;

	}

	cvReleaseMemStorage(&BOUNDING.storage);
	BOUNDING.valid = true;
}

void Compute_CONVEX(IplImage* roi)
{
	if(CONVEX.valid) return;

	CONVEX.maxcavity = 0;
	CONVEX.num_cavities = 0;
	CONVEX.sumcavity = 0;

	CvMemStorage* storage1;
	CvMemStorage* storage2;
	CvSeq* contours;
	CvSeq* chull;
	CvSeq* defects;


	storage1 = cvCreateMemStorage(0);
	storage2 = cvCreateMemStorage(0);
	contours = 0;
	int contours_num = cvFindContours(roi, storage1, &contours, sizeof(CvContour), CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cvPoint(0,0));
	if(contours_num > 0)
	{

//		chull = cvContourConvexHull(contours, CV_COUNTER_CLOCKWISE, storage2);
		chull = cvConvexHull2(contours, 0, CV_CLOCKWISE, 0);  


		if(chull->total > 4)
		{
			double perim = 0.0;
			double area = 0.0;

			CvPoint pt0 = **CV_GET_SEQ_ELEM( CvPoint*, chull, chull->total - 1 );
			for(int i = 0; i < chull->total; i++ )
			{
				CvPoint pt = **CV_GET_SEQ_ELEM( CvPoint*, chull, i );
				double xx = pt.x-pt0.x;
				xx *= xx;
				double yy = pt.y-pt0.y;
				yy *= yy;
				perim += sqrt(xx+yy);
				area += (pt.x*pt0.y - pt0.x*pt.y); 
				pt0 = pt;
			}
			CONVEX.perim = perim;
			CONVEX.area = area/2;

//			CONVEX.perim = cvArcLength(chull);
//			CONVEX.area = cvContourArea(chull);

			defects = cvConvexityDefects(contours, chull, storage2); 
			//for(;defects;defects = defects->h_next)
			{
				int nomdef = defects->total;
				if(nomdef != 0)
				{
					double max_depth = 0.0;
					double sum_depth = 0.0;

					CvConvexityDefect* defectArray;
					defectArray = (CvConvexityDefect*) malloc(sizeof(CvConvexityDefect)*nomdef);
					cvCvtSeqToArray(defects, defectArray, CV_WHOLE_SEQ);
					for(int i=0; i<nomdef; i++)
					{
						sum_depth+=defectArray[i].depth;
						if(max_depth < defectArray[i].depth) max_depth = defectArray[i].depth;
					}
					CONVEX.maxcavity = max_depth;
					CONVEX.sumcavity = sum_depth;
					CONVEX.num_cavities = nomdef;
					free(defectArray);
				}
			}
		}
	}


	cvReleaseMemStorage(&storage2);
	cvReleaseMemStorage(&storage1);
	CONVEX.valid = true;
}

void Compute_BASIC(IplImage* roi)
{
	if(BASIC.valid) return;
	unsigned int xx = 0;
	unsigned int yy = 0;
	unsigned int aa = 0;
	for(int y = 0; y < roi->height; y++)
	for(int x = 0; x < roi->width; x++)
	{
//		CvConnectedComp comp;
		CvScalar g = cvGet2D(roi, y, x);
		if(g.val[0] > 0)
		{	
			xx += x;
			yy += y;
			aa++;
		}
	}
	BASIC.X = (double)xx/aa;
	BASIC.Y = (double)yy/aa;
	BASIC.F = (double)aa;
	BASIC.Spol = sqrt((double)aa);
	BASIC.valid = true;
}

double ComputeGeomFeature(unsigned int x, unsigned int y, char* name, IplImage* roi)
{
	if(strcmp(name, "GeoX") == 0) {Compute_BASIC(roi); return BASIC.X;}
	if(strcmp(name, "GeoY") == 0) {Compute_BASIC(roi); return BASIC.Y;}
	if(strcmp(name, "GeoF") == 0) {Compute_BASIC(roi); return BASIC.F;}
	if(strcmp(name, "GeoSpol") == 0) {Compute_BASIC(roi); return BASIC.Spol;}

	if(strcmp(name, "GeoS") == 0) {Compute_BOUNDING(roi); return BOUNDING.box_rect.size.height;}
	if(strcmp(name, "GeoL") == 0) {Compute_BOUNDING(roi); return BOUNDING.box_rect.size.width;}
	if(strcmp(name, "GeoSxL") == 0) {Compute_BOUNDING(roi); return BOUNDING.box_rect.size.width*BOUNDING.box_rect.size.height;}
	if(strcmp(name, "GeoSoL") == 0) 
	{
		Compute_BOUNDING(roi); 
		if(BOUNDING.box_rect.size.height > 0) return BOUNDING.box_rect.size.width/BOUNDING.box_rect.size.height;
		return mznan;
	}
	if(strcmp(name, "GeoSxLoF") == 0) {
		Compute_BASIC(roi); 
		Compute_BOUNDING(roi); 
		if(BASIC.F > 0) return BOUNDING.box_rect.size.width*BOUNDING.box_rect.size.height/BASIC.F;
		return mznan;
	}
	if(strcmp(name, "GeoNCont") == 0) {Compute_BOUNDING(roi); return BOUNDING.contours_num;}

	if(strcmp(name, "GeoEmax") == 0) {Compute_BOUNDING(roi); return BOUNDING.box_elli.size.height;}
	if(strcmp(name, "GeoEmin") == 0) {Compute_BOUNDING(roi); return BOUNDING.box_elli.size.width;}
	if(strcmp(name, "GeoEoE") == 0) 
	{
		Compute_BOUNDING(roi); 
		if(BOUNDING.box_elli.size.height > 0) return BOUNDING.box_elli.size.width/BOUNDING.box_elli.size.height;
		return mznan;
	}
	if(strcmp(name, "GeoExE") == 0) 
	{
		Compute_BOUNDING(roi); 
		return BOUNDING.box_elli.size.width*BOUNDING.box_elli.size.height;
	}
	if(strcmp(name, "GeoEAox") == 0) {Compute_BOUNDING(roi); return BOUNDING.box_elli.angle;}
	if(strcmp(name, "GeoRAox") == 0) {Compute_BOUNDING(roi); return BOUNDING.box_rect.angle;}

	if(strcmp(name, "GeoEcd") == 0) 
	{
		Compute_BASIC(roi); Compute_BOUNDING(roi); 
		double xx = (BOUNDING.box_elli.center.x - BASIC.X); xx *= xx;
		double yy = (BOUNDING.box_elli.center.y - BASIC.Y); yy *= yy;
		return sqrt(xx+yy);
	}
	if(strcmp(name, "GeoRound") == 0) 
	{
		Compute_BASIC(roi); Compute_BOUNDING(roi); 
		double d = BOUNDING.perim * BOUNDING.perim;
		if(d > 0) return BASIC.F/d;
		return mznan;
	}
	if(strcmp(name, "GeoRCen") == 0) 
	{
		Compute_BASIC(roi); Compute_BOUNDING(roi); 
		double xx = (BOUNDING.box_rect.center.x - BASIC.X); xx *= xx;
		double yy = (BOUNDING.box_rect.center.y - BASIC.Y); yy *= yy;
		return sqrt(xx+yy);
	}
	if(strcmp(name, "GeoFt") == 0) {Compute_BOUNDING(roi); return BOUNDING.area;}
	if(strcmp(name, "GeoFtoF") == 0) 
	{
		Compute_BOUNDING(roi); 
		if(BASIC.F > 0) return BOUNDING.area/BASIC.F;
		return mznan;
	}
	if(strcmp(name, "GeoUl") == 0) {Compute_BOUNDING(roi); return BOUNDING.perim;}
	if(strcmp(name, "GeoD2") == 0) {Compute_BOUNDING(roi); return BOUNDING.radius*2.0;}

	if(strcmp(name, "GeoCM00") == 0) {Compute_MOMENTS(roi); return cvGetCentralMoment(& MOMENTS.moments, 0, 0);}
	if(strcmp(name, "GeoCM10") == 0) {Compute_MOMENTS(roi); return cvGetCentralMoment(& MOMENTS.moments, 1, 0);}
	if(strcmp(name, "GeoCM01") == 0) {Compute_MOMENTS(roi); return cvGetCentralMoment(& MOMENTS.moments, 0, 1);}
	if(strcmp(name, "GeoCM11") == 0) {Compute_MOMENTS(roi); return cvGetCentralMoment(& MOMENTS.moments, 1, 1);}
	if(strcmp(name, "GeoCM20") == 0) {Compute_MOMENTS(roi); return cvGetCentralMoment(& MOMENTS.moments, 2, 0);}
	if(strcmp(name, "GeoCM02") == 0) {Compute_MOMENTS(roi); return cvGetCentralMoment(& MOMENTS.moments, 0, 2);}
	if(strcmp(name, "GeoCM21") == 0) {Compute_MOMENTS(roi); return cvGetCentralMoment(& MOMENTS.moments, 2, 1);}
	if(strcmp(name, "GeoCM12") == 0) {Compute_MOMENTS(roi); return cvGetCentralMoment(& MOMENTS.moments, 1, 2);}
	if(strcmp(name, "GeoCM30") == 0) {Compute_MOMENTS(roi); return cvGetCentralMoment(& MOMENTS.moments, 3, 0);}
	if(strcmp(name, "GeoCM03") == 0) {Compute_MOMENTS(roi); return cvGetCentralMoment(& MOMENTS.moments, 0, 3);}

	if(strcmp(name, "GeoNCM10") == 0) {Compute_MOMENTS(roi); return cvGetNormalizedCentralMoment(& MOMENTS.moments, 1, 0);}
	if(strcmp(name, "GeoNCM01") == 0) {Compute_MOMENTS(roi); return cvGetNormalizedCentralMoment(& MOMENTS.moments, 0, 1);}
	if(strcmp(name, "GeoNCM11") == 0) {Compute_MOMENTS(roi); return cvGetNormalizedCentralMoment(& MOMENTS.moments, 1, 1);}
	if(strcmp(name, "GeoNCM20") == 0) {Compute_MOMENTS(roi); return cvGetNormalizedCentralMoment(& MOMENTS.moments, 2, 0);}
	if(strcmp(name, "GeoNCM02") == 0) {Compute_MOMENTS(roi); return cvGetNormalizedCentralMoment(& MOMENTS.moments, 0, 2);}
	if(strcmp(name, "GeoNCM21") == 0) {Compute_MOMENTS(roi); return cvGetNormalizedCentralMoment(& MOMENTS.moments, 2, 1);}
	if(strcmp(name, "GeoNCM12") == 0) {Compute_MOMENTS(roi); return cvGetNormalizedCentralMoment(& MOMENTS.moments, 1, 2);}
	if(strcmp(name, "GeoNCM30") == 0) {Compute_MOMENTS(roi); return cvGetNormalizedCentralMoment(& MOMENTS.moments, 3, 0);}
	if(strcmp(name, "GeoNCM03") == 0) {Compute_MOMENTS(roi); return cvGetNormalizedCentralMoment(& MOMENTS.moments, 0, 3);}

	if(strcmp(name, "GeoConvDef") == 0) {Compute_CONVEX(roi); return CONVEX.num_cavities;}
	if(strcmp(name, "GeoConvMxDpth") == 0) {Compute_CONVEX(roi); return CONVEX.maxcavity;}
	if(strcmp(name, "GeoConvSmDpth") == 0) {Compute_CONVEX(roi); return CONVEX.sumcavity;}
	if(strcmp(name, "GeoConvAvDpth") == 0) 
	{
		Compute_CONVEX(roi); 
		if(CONVEX.num_cavities > 0) return CONVEX.sumcavity/CONVEX.num_cavities;
		return mznan;
	}
	if(strcmp(name, "GeoConvMxDoF") == 0) 
	{
		Compute_CONVEX(roi); Compute_BASIC(roi);
		if(BASIC.F > 0) return CONVEX.sumcavity*CONVEX.sumcavity/BASIC.F;
		return mznan;
	}
	if(strcmp(name, "GeoConvPeri") == 0) 
	{
		Compute_CONVEX(roi); Compute_BOUNDING(roi);
		if(BOUNDING.perim > 0) return CONVEX.perim / BOUNDING.perim;
		return mznan;
	}
	if(strcmp(name, "GeoConvF") == 0){Compute_CONVEX(roi); return CONVEX.area;}
	if(strcmp(name, "GeoConvFoF") == 0) 
	{
		Compute_CONVEX(roi); Compute_BOUNDING(roi);
		if(BOUNDING.area > 0) return CONVEX.area / BOUNDING.area ;
		return mznan;
	}

	return mznan;
}