/*
* Segment - Image segmentation and object identification
*
* Copyright 2013 Piotr M. Szczypinski <piotr.szczypinski@p.lodz.pl>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <opencv2/highgui/highgui_c.h>
#include <opencv2/imgproc/imgproc_c.h>

char seg_channel = 'Y';
int seg_threshold = 102;
int seg_selem = 15;
int seg_minarea = 4000;
int seg_maxarea = 4000000;
bool seg_saveseparate = false;


char* input = 0;
char* output = 0;


const CvScalar RoiColors[16] =
{
    {0.0, 0.0, 255.0, 0.0},
    {0.0, 255.0, 0.0, 0.0},
    {255.0, 0.0, 0.0, 0.0},
    {255.0, 255.0, 0.0, 0.0},
    {255.0, 0.0, 255.0, 0.0},
    {0.0, 255.0, 255.0, 0.0},
    {0.0, 128.0, 255.0, 0.0},
    {128.0, 0.0, 255.0, 0.0},
    {0.0, 255.0, 128.0, 0.0},
    {128.0, 255.0, 0.0, 0.0},
    {255.0, 0.0, 128.0, 0.0},
    {255.0, 128.0, 0.0, 0.0},
    {0.0, 196.0, 255.0, 0.0},
    {0.0, 255.0, 196.0, 0.0},
    {255.0, 0.0, 196.0, 0.0},
    {196.0, 255.0, 0.0, 0.0}
};


void printhelp(void)
{
    printf("Usage: segment [OPTION]...\n");
    printf("Segment image and identify objects.\n\n");

    printf("Options:\n");
    printf("\t-channel Y, R, G or B\n");
    printf("\t-threshold GRAY_SCALE_THRESHOLD\n");
    printf("\t-thickness OPENING_CLOSING_DEPTH\n");
    printf("\t-maxarea OBJECT_MAXIMUM_AREA\n");
    printf("\t-minarea OBJECT_MINIMUM_AREA\n");
    printf("\t-input INPUT_IMAGE_FILE_NAME\n");
    printf("\t-output OUTPUT_FILE_NAMES_STUB\n");
    printf("\t-separate\n\n");

    printf("Example:\n");
    printf("\tsegment -input salami.tif\n");
    printf("\tsegment -threshold 128 -minarea 1000 -input salami.tif -separate -output salami_roi\n");
    printf("\n");
}

int scan_parameters(int argc, char* argv[])
{
    int argi = 1;
    while(argi < argc)
    {
        if(strcmp(argv[argi], "-threshold")==0)
        {
            argi++;
            if(sscanf(argv[argi], "%i", &seg_threshold) != 1) return argi;
        }
        else if(strcmp(argv[argi], "-thickness")==0)
        {
            argi++;
            if(sscanf(argv[argi], "%i", &seg_selem) != 1) return argi;
        }
        else if(strcmp(argv[argi], "-channel")==0)
        {
            argi++;
            if(sscanf(argv[argi], "%c", &seg_channel) != 1) return argi;
        }
        else if(strcmp(argv[argi], "-minarea")==0)
        {
            argi++;
            if(sscanf(argv[argi], "%i", &seg_minarea) != 1) return argi;
        }
        else if(strcmp(argv[argi], "-maxarea")==0)
        {
            argi++;
            if(sscanf(argv[argi], "%i", &seg_maxarea) != 1) return argi;
        }
        else if(strcmp(argv[argi], "-input")==0)
        {
            argi++;
            input = argv[argi];
            if(output == 0) output = argv[argi];
        }
        else if(strcmp(argv[argi], "-output")==0)
        {
            argi++;
            output = argv[argi];
        }
        else if(strcmp(argv[argi], "-separate")==0)
        {
            seg_saveseparate = true;
        }
        argi++;
    }
    return 0;
}



int main(int argc, char* argv[])
{
    char str[1024];

    IplImage* src = 0;
    IplImage* buf = 0;
    IplImage* bfu = 0;
    IplImage* dst = 0;

    IplConvKernel* selem = 0;

    int ret = scan_parameters(argc, argv);
    if(ret != 0 || input == 0 || output == 0)
    {
        printhelp();
        return ret;
    }

    if( (src = cvLoadImage(input,1)) == 0 )
    {
        printf("Cannot load image: %s\n", argv[1]);
        return -1;
    }

    bfu = cvCreateImage(cvSize(src->width, src->height), IPL_DEPTH_8U, 1);
    dst = cvCreateImage(cvSize(src->width, src->height), IPL_DEPTH_8U, 3);

    if(seg_channel == 'R' || seg_channel == 'G' || seg_channel == 'B')
    {
        IplImage* ir = cvCreateImage(cvSize(src->width, src->height), IPL_DEPTH_8U, 1);
        IplImage* ig = cvCreateImage(cvSize(src->width, src->height), IPL_DEPTH_8U, 1);
        IplImage* ib = cvCreateImage(cvSize(src->width, src->height), IPL_DEPTH_8U, 1);

        cvSplit(src, ib, ig, ir, NULL);
        switch(seg_channel)
        {
        case 'R': buf = ir; cvReleaseImage(&ib); cvReleaseImage(&ig); break;
        case 'G': buf = ig; cvReleaseImage(&ib); cvReleaseImage(&ir); break;
        case 'B': buf = ib; cvReleaseImage(&ir); cvReleaseImage(&ig); break;
        }
    }
    else
    {
        buf = cvCreateImage(cvSize(src->width, src->height), IPL_DEPTH_8U, 1);
        cvCvtColor(src, buf, CV_BGR2GRAY);
    }

//Progowanie
    cvThreshold(buf, bfu, seg_threshold, 255.0, CV_THRESH_BINARY);


//Wygladzanie
    selem = cvCreateStructuringElementEx(seg_selem, seg_selem, seg_selem/2, seg_selem/2, CV_SHAPE_ELLIPSE);

    cvErode(bfu, buf, selem, 1);
    cvDilate(buf, bfu, selem, 1);
    cvDilate(bfu, buf, selem, 1);
    cvErode(buf, bfu, selem, 1);

//Identyfikacja
    int objects = 0;
    for(int y = 0; y < src->height; y++)
    for(int x = 0; x < src->width; x++)
    {
        CvConnectedComp comp;
        CvScalar g = cvGet2D(bfu, y, x);
        if(g.val[0] >= 128)
        {
            cvFloodFill(bfu, cvPoint(x, y), cvScalar(63.0, 0.0, 0.0), cvScalarAll(0), cvScalarAll(0), &comp); // int flags=4, CvArr* mask=NULL)
            if(comp.area < seg_minarea || comp.area > seg_maxarea) cvFloodFill(bfu, cvPoint(x, y), cvScalar(0.0, 0.0, 0.0));
            else objects++;
        }
    }
    printf("Input: %s\n", input);
    printf("Count: %.4i\n\n", objects);

    cvCopy(bfu, buf);


//Zapis
    int r = 0;
    cvCvtColor(buf, dst, CV_GRAY2BGR);


    if(seg_saveseparate)
    {
        for(int y = 0; y < src->height; y++)
        for(int x = 0; x < src->width; x++)
        {
            CvConnectedComp comp;
            CvScalar g = cvGet2D(buf, y, x);
            if(g.val[0] >= 32.0)
            {
                cvFloodFill(buf, cvPoint(x, y), cvScalar(255.0, 0.0, 0.0), cvScalarAll(0), cvScalarAll(0), &comp);
                cvSetImageROI(buf, comp.rect);
                cvSetImageROI(src, comp.rect);

                sprintf(str, "%s%.4i.bmp", output, r);
                cvSaveImage(str, src);
                sprintf(str, "Roi_%s%.4i.bmp", output, r);
                cvSaveImage(str, buf);

                cvResetImageROI(src);
                cvResetImageROI(buf);
                cvFloodFill(buf, cvPoint(x, y), cvScalar(0.0, 0.0, 0.0), cvScalarAll(0), cvScalarAll(0), &comp);
                r++;
            }
        }
    }
    else
    {
        for(int y = 0; y < src->height; y++)
        for(int x = 0; x < src->width; x++)
        {
            CvConnectedComp comp;
            CvScalar g = cvGet2D(buf, y, x);
            if(g.val[0] >= 32.0)
            {
                cvFloodFill(buf, cvPoint(x, y), cvScalar(0.0, 0.0, 0.0), cvScalarAll(0), cvScalarAll(0), &comp);
                cvFloodFill(dst, cvPoint(x, y), RoiColors[r%16], cvScalarAll(0), cvScalarAll(0), &comp);

                if(r%16 == 15 || r == objects-1)
                {
                    sprintf(str, "%s%.4i.bmp", output, r/16);
                    cvSaveImage(str, dst);
                    cvCvtColor(bfu, dst, CV_GRAY2BGR);
                }
                if(r >= objects-1)
                {
                    y = src->height;
                    x = src->width;
                }
                r++;
            }
        }
    }
    cvReleaseImage(&dst);
    cvReleaseImage(&bfu);
    cvReleaseImage(&buf);
    cvReleaseImage(&src);
    return 0;
}

